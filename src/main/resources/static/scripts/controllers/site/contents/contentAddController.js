app.controller('contentAddCtrl',['$scope','apiHandler','textAngularManager',
    function ($scope,apiHandler,textAngularManager) {
    $scope.data={};
    $scope.addData=()=>{
        let url='content/';
        if ($scope.data.key==null||$scope.data.key==undefined||$scope.data.key=="") {
            Swal.fire('please enter key')
            return;
        }
        apiHandler.callPost(url,$scope.data,(response)=>{
        $scope.changeMenu('content-list');
        },(error)=>{

        },true)
    }
}]);