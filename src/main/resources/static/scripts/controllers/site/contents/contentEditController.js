app.controller('contentEditCtrl',function ($scope,apiHandler,$rootScope) {
    $scope.data={};
    $scope.id=$rootScope.dataId;
    $scope.editData=()=>{
        let url='content/';
        if ($scope.data.key==null||$scope.data.key==undefined||$scope.data.key==""){
            Swal.fire('please enter key')
            return;
        }
        apiHandler.callPut(url,$scope.data,(response)=>{
        $scope.changeMenu('content-list');
        },(error)=>{

        },true)
    }
    $scope.getData=()=>{
        apiHandler.callGet("content/"+$scope.id,(response)=>{
            debugger;
            $scope.data=response.dataList[0];
        },(error)=>{

        },true)
    }
    $scope.getData();
});