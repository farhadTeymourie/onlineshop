app.controller('navListCtrl',function ($scope,apiHandler,$rootScope) {
    // get data by pagination and parameter value
    $scope.query={
    pageSize:5,
    pageNumber:0
    };
    $scope.totalCount=0;
    $scope.pageCount=0;
    //get and show data List in page
    $scope.dataList=[];
    $scope.getDataList=()=>{
        let url='nav/getAll?pageSize='+$scope.query.pageSize+'&pageNumber='+$scope.query.pageNumber;
        apiHandler.callGet(url,(response)=>{
        $scope.dataList=response.dataList;
        //calculate item for show in per page
        $scope.totalCount=response.totalCount;
        $scope.pageCount=$scope.totalCount/$scope.query.pageSize;
        $scope.pageCount=parseInt($scope.pageCount);
        if ($scope.totalCount % $scope.query.pageSize>0){
            $scope.pageCount++;
        }
        },(error)=>{

        },true);
    }
    //change page for pagination
    $scope.changePage=(pageNumber)=>{
        $scope.query.pageNumber=pageNumber;
        $scope.getDataList();
    }
    //for use ng-repeat we used array
    $scope.range=(max)=>{
      return new Array(max);
    }
    //return id for get data and send to navEditController for  edit data
    $scope.editItem=(id)=>{
        $rootScope.dataId=id;
        $scope.changeMenu('nav-Edit');
    }
    $scope.deleteItem=(id)=> {
        //sweetalert
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                debugger;
                apiHandler.callDelete('nav/' + id, (response) => {
                    Swal.fire(
                        'Deleted!',
                        'Your data has been deleted.',
                        'success'
                    )
                    $scope.getDataList();
                }, (error) => {

                }, true);

            }
        })
    }
        //sweetalert

        //changeItem by itemOrder
    $scope.changeOrder = (id,direction) =>{
        Swal.fire({
            title: 'Are you sure?',
            text: "Do you want to change order?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, change it!'
        }).then((result) => {
            if (result.isConfirmed) {
                apiHandler.callPost('nav/changeOrder/'+id + '/'+direction,null,(response)=>{
                    Swal.fire(
                        'Changed!',
                        'Your data has been changed.',
                        'success'
                   );
                    $scope.getDataList();
                },(error)=>{
                },true);
           }
       })
    }
        $scope.getDataList();
    });