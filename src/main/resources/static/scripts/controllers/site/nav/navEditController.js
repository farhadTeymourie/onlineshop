app.controller('navEditCtrl',function ($scope,apiHandler,$rootScope) {
    $scope.data={};
    $scope.id=$rootScope.dataId;
    $scope.editData=()=>{
        debugger;
        let url='nav/';
        if ($scope.data.title==null||$scope.data.title==undefined||$scope.data.title==""){
            Swal.fire('please enter title')
            return;
        }
        if ($scope.data.link==null||$scope.data.link==undefined||$scope.data.link==""){
            Swal.fire('please enter link')
            return;
        }
        if ($scope.data.enable==null||$scope.data.enable==undefined||$scope.data.enable==""){
            Swal.fire('please set enable')
            return;
        }
        apiHandler.callPut(url,$scope.data,(response)=>{
        $scope.changeMenu('nav-list');
        },(error)=>{

        },true)
    }
    $scope.getData=()=>{
        debugger;
        apiHandler.callGet("nav/"+$scope.id,(response)=>{
            debugger;
            $scope.data=response.dataList[0];
        },(error)=>{

        },true)
    }
    $scope.getData();
});