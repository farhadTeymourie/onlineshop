app.controller('sliderEditCtrl', function ($scope, apiHandler, $rootScope) {
    $scope.data = {};
    $scope.id = $rootScope.dataId;
    $scope.editData = () => {
        if ($rootScope.uploadedFile != null && $rootScope.uploadedFile != "" &&
            $rootScope.uploadedFile != undefined)
            $scope.data.imageName = $rootScope.uploadedFile;
        let url = 'slider/';
        if ($scope.data.title==null||$scope.data.title==undefined||$scope.data.title==""){
            Swal.fire('please enter title')
            return;
        }
        if ($scope.data.link==null||$scope.data.link==undefined||$scope.data.link==""){
            Swal.fire('please enter link')
            return;
        }
        if ($scope.data.enable==null||$scope.data.enable==undefined||$scope.data.enable==""){
            Swal.fire('please set Enable field')
            return;
        }
        if ($scope.data.description==null||$scope.data.description==undefined||$scope.data.description==""){
            Swal.fire('please set description field')
            return;
        }
        if ($scope.data.imageName == undefined || $scope.data.imageName == null || $scope.data.imageName == '') {
            Swal.fire('please upload an image')
            return;
        }
        apiHandler.callPut(url, $scope.data, (response) => {
            $scope.changeMenu('slider-list');
            $rootScope.uploadedFile=null;
        }, (error) => {

        }, true)
    }
    $scope.getData = () => {
        debugger;
        apiHandler.callGet("slider/" + $scope.id, (response) => {
            debugger;
            $scope.data = response.dataList[0];
        }, (error) => {

        }, true)
    }
    $scope.getData();
});