app.controller('blogEditCtrl', function ($scope, apiHandler, $rootScope) {
    $scope.data = {};
    $scope.id = $rootScope.dataId;
    $scope.editData = () => {
        if ($rootScope.uploadedFile != null && $rootScope.uploadedFile != "" &&
            $rootScope.uploadedFile != undefined)
            $scope.data.image = $rootScope.uploadedFile;
        let url = 'blog/';
        if ($scope.data.title==null||$scope.data.title==undefined||$scope.data.title==""){
            Swal.fire('please enter title')
            return;
        }
        if ($scope.data.status==null||$scope.data.status==undefined||$scope.data.status==""){
            Swal.fire('please enter status')
            return;
        }
        if ($scope.data.publishDate==null||$scope.data.publishDate==undefined||$scope.data.publishDate==""){
            Swal.fire('please set publishDate field')
            return;
        }
        if ($scope.data.description==null||$scope.data.description==undefined||$scope.data.description==""){
            Swal.fire('please set description field')
            return;
        }
        if ($scope.data.image == undefined || $scope.data.image == null || $scope.data.image == '') {
            Swal.fire('please upload an image')
            return;
        }
        apiHandler.callPut(url, $scope.data, (response) => {
            $scope.changeMenu('blog-list');
            $rootScope.uploadedFile=null;
        }, (error) => {

        }, true)
    }
    $scope.getData = () => {
        debugger;
        apiHandler.callGet("blog/" + $scope.id, (response) => {
            debugger;
            $scope.data = response.dataList[0];
        }, (error) => {

        }, true)
    }
    $scope.getData();
});