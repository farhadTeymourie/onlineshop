app.controller('blogAddCtrl',function ($scope,apiHandler,$rootScope) {
    $scope.data={};
    $scope.addData=()=>{
        let url='blog/';
        $scope.data.image = $rootScope.uploadedFile;
        if ($scope.data.title==null||$scope.data.title==undefined||$scope.data.title==""){
            Swal.fire('please enter title')
            return;
        }
        if ($scope.data.publishDate==null||$scope.data.publishDate==undefined||$scope.data.publishDate==""){
            Swal.fire('please set publishDate field')
            return;
        }
        if ($scope.data.description==null||$scope.data.description==undefined||$scope.data.description==""){
            Swal.fire('please set description field')
            return;
        }
        // if ($scope.data.image == undefined || $scope.data.image == null || $scope.data.image == '') {
        //     Swal.fire('please upload an image')
        //     return;
        // }
        apiHandler.callPost(url,$scope.data,(response)=>{
            debugger;
        $scope.changeMenu('blog-list');
        },(error)=>{

        },true)
    }
});