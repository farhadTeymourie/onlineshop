app.controller('userAddCtrl',function ($scope,apiHandler) {
    $scope.data={};
    $scope.addData=()=>{
        let url='user/';
        if ($scope.data.firstName==null||$scope.data.firstName==undefined||$scope.data.firstName==""){
            Swal.fire('please enter firstName')
            return;
        }
        if ($scope.data.lastName==null||$scope.data.lastName==undefined||$scope.data.lastName==""){
            Swal.fire('please enter lastName')
            return;
        }
        if ($scope.data.email==null||$scope.data.email==undefined||$scope.data.email==""){
            Swal.fire('please enter email')
            return;
        }
        if ($scope.data.userName==null||$scope.data.userName==undefined||$scope.data.userName==""){
            Swal.fire('please enter username')
            return;
        }
        if ($scope.data.password==null||$scope.data.password==undefined||$scope.data.password==""){
            Swal.fire('please enter password')
            return;
        }
        if ($scope.data.role==null||$scope.data.role==undefined||$scope.data.role==""){
            Swal.fire('please set role field')
            return;
        }
        debugger;
        apiHandler.callPost(url,$scope.data,(response)=>{
            debugger;
        $scope.changeMenu('user-list');
        },(error)=>{

        },true)
    }
});