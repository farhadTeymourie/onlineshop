app.controller('userEditCtrl',function ($scope,apiHandler,$rootScope) {
    $scope.data={};
    $scope.id=$rootScope.dataId;
    $scope.editData=()=>{
        debugger;
        let url='user/';
        if ($scope.data.firstName==null||$scope.data.firstName==undefined||$scope.data.firstName==""){
            Swal.fire('please enter firstName')
            return;
        }
        if ($scope.data.lastName==null||$scope.data.lastName==undefined||$scope.data.lastName==""){
            Swal.fire('please enter lastName')
            return;
        }
        if ($scope.data.email==null||$scope.data.email==undefined||$scope.data.email==""){
            Swal.fire('please enter email')
            return;
        }
        if ($scope.data.userName==null||$scope.data.userName==undefined||$scope.data.userName==""){
            Swal.fire('please enter username')
            return;
        }
         apiHandler.callPut(url,$scope.data,(response)=>{
        $scope.changeMenu('user-list');
        },(error)=>{

        },true)
    }
    $scope.getData=()=>{
        debugger;
        apiHandler.callGet("user/"+$scope.id,(response)=>{
            debugger;
            $scope.data=response.dataList[0];
        },(error)=>{

        },true)
    }
    $scope.getData();
});