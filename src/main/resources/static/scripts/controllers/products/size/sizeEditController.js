app.controller('sizeEditCtrl',function ($scope,apiHandler,$rootScope) {
    $scope.data={};
    $scope.id=$rootScope.dataId;
    $scope.editData=()=>{
        debugger;
        let url='size/';
        if ($scope.data.title==null||$scope.data.title==undefined||$scope.data.title==""){
            Swal.fire('please enter title')
            return;
        }
        if ($scope.data.description==null||$scope.data.description==undefined||$scope.data.description==""){
            Swal.fire('please enter description')
            return;
        }
        apiHandler.callPut(url,$scope.data,(response)=>{
        $scope.changeMenu('size-list');
        },(error)=>{

        },true)
    }
    $scope.getData=()=>{
        debugger;
        apiHandler.callGet("size/"+$scope.id,(response)=>{
            debugger;
            $scope.data=response.dataList[0];
        },(error)=>{

        },true)
    }
    $scope.getData();
});