app.controller('sizeAddCtrl',function ($scope,apiHandler) {
    $scope.data={};
    $scope.addData=()=>{
        let url='size/';
        if ($scope.data.title==null||$scope.data.title==undefined||$scope.data.title==""){
            Swal.fire('please enter title')
            return;
        }
        if ($scope.data.description==null||$scope.data.description==undefined||$scope.data.description==""){
            Swal.fire('please enter description')
            return;
        }
        apiHandler.callPost(url,$scope.data,(response)=>{
            debugger;
        $scope.changeMenu('size-list');
        },(error)=>{

        },true)
    }
});