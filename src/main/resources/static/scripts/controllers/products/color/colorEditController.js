app.controller('colorEditCtrl',function ($scope,apiHandler,$rootScope) {
    $scope.data={};
    $scope.id=$rootScope.dataId;
    $scope.editData=()=>{
        debugger;
        let url='color/';
        if ($scope.data.name==null||$scope.data.name==undefined||$scope.data.name==""){
            Swal.fire('please enter name')
            return;
        }
        if ($scope.data.value==null||$scope.data.value==undefined||$scope.data.value==""){
            Swal.fire('please enter value')
            return;
        }
        apiHandler.callPut(url,$scope.data,(response)=>{
        $scope.changeMenu('color-list');
        },(error)=>{

        },true)
    }
    $scope.getData=()=>{
        debugger;
        apiHandler.callGet("color/"+$scope.id,(response)=>{
            debugger;
            $scope.data=response.dataList[0];
        },(error)=>{

        },true)
    }
    $scope.getData();
});