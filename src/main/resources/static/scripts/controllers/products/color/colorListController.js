app.controller('colorListCtrl',function ($scope,apiHandler,$rootScope) {
    // get data by pagination and parameter value
    $scope.query={
    pageSize:5,
    pageNumber:0
    };
    $scope.totalCount=0;
    $scope.pageCount=0;
    //get and show data List in page
    $scope.dataList=[];
    $scope.getDataList=()=>{
        let url='color/getAll?pageSize='+$scope.query.pageSize+'&pageNumber='+$scope.query.pageNumber;
        apiHandler.callGet(url,(response)=>{
        $scope.dataList=response.dataList;
        //calculate item for show in per page
        $scope.totalCount=response.totalCount;
        $scope.pageCount=$scope.totalCount/$scope.query.pageSize;
        $scope.pageCount=parseInt($scope.pageCount);
        if ($scope.totalCount % $scope.query.pageSize>0){
            $scope.pageCount++;
        }
        },(error)=>{

        },true);
    }
    //change page for pagination
    $scope.changePage=(pageNumber)=>{
        $scope.query.pageNumber=pageNumber;
        $scope.getDataList();
    }
    //for use ng-repeat we used array
    $scope.range=(max)=>{
      return new Array(max);
    }
    //return id for get data and send to colorEditController for  edit data
    $scope.editItem=(id)=>{
        $rootScope.dataId=id;
        $scope.changeMenu('color-Edit');
    }
    $scope.getDataList();
    });