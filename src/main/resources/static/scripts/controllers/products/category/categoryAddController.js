app.controller('categoryAddCtrl',function ($scope,apiHandler,$rootScope) {
    $scope.data={};
    $scope.addData=()=>{
        let url='category/';
        $scope.data.image = $rootScope.uploadedFile;
        if ($scope.data.title==null||$scope.data.title==undefined||$scope.data.title==""){
            Swal.fire('please enter title')
            return;
        }
        if ($scope.data.enable==null||$scope.data.enable==undefined||$scope.data.enable==""){
            Swal.fire('please set Enable field')
            return;
        }
        if ($scope.data.description==null||$scope.data.description==undefined||$scope.data.description==""){
            Swal.fire('please set description field')
            return;
        }
        if ($scope.data.image == undefined || $scope.data.image == null || $scope.data.image == "") {
            debugger;
            Swal.fire('please upload an image')
            return;
        }
        apiHandler.callPost(url,$scope.data,(response)=>{
            debugger;
        $scope.changeMenu('category-list');
        },(error)=>{

        },true)
    }
});