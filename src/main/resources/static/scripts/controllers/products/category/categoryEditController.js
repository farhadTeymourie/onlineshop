app.controller('categoryEditCtrl', function ($scope, apiHandler, $rootScope) {
    $scope.data = {};
    $scope.id = $rootScope.dataId;
    $scope.editData = () => {
        debugger;
        if ($rootScope.uploadedFile != null && $rootScope.uploadedFile != "" &&
            $rootScope.uploadedFile != undefined)
            $scope.data.image = $rootScope.uploadedFile;
        let url = 'category/';
        if ($scope.data.title==null||$scope.data.title==undefined||$scope.data.title==""){
            Swal.fire('please enter title')
            return;
        }
        if ($scope.data.enable==null||$scope.data.enable==undefined||$scope.data.enable==""){
            Swal.fire('please set Enable field')
            return;
        }
        if ($scope.data.description==null||$scope.data.description==undefined||$scope.data.description==""){
            Swal.fire('please set description field')
            return;
        }
        if ($scope.data.image == undefined || $scope.data.image == null || $scope.data.image == '') {
            Swal.fire('please upload an image')
            return;
        }
        apiHandler.callPut(url, $scope.data, (response) => {
            $scope.changeMenu('category-list');
            $rootScope.uploadedFile=null;
        }, (error) => {

        }, true)
    }
    $scope.getData = () => {
         apiHandler.callGet("category/" + $scope.id, (response) => {
            debugger;
            $scope.data = response.dataList[0];
        }, (error) => {

        }, true)
    }
    $scope.getData();
});