app.controller("panelCtlr", function ($scope, apiHandler,$cookies,$rootScope) {

    $scope.template="views/dashboard.html";
    $scope.templateName="dashboard";

    $scope.checkAccess=()=>{
        var token=$cookies.get("userToken");
        if (token==null||token==undefined||token==""){
            location.href="/login";
            return;
        }
        debugger
        $scope.getUserInfo();
    }
    $scope.getUserInfo=()=>{
        apiHandler.callGet('user/getUserInfo',(response)=>{
            $rootScope.userInfo=response.dataList[0];
            debugger;
            $scope.user=$rootScope.userInfo;
        },(error)=>{

        },true)
    }
    $scope.changeMenu=(templateName)=>{
        $scope.templateName=templateName;
        $scope.template=$scope.getMenuPrefix(templateName);
        $scope.templateGroup=$scope.getTemplateGroup(templateName);
    }
    $scope.getMenuPrefix = (templateName) => {
        if (templateName === 'dashboard') {
            return 'views/' + templateName + '.html';
        } else if (templateName === 'nav-list' || templateName === 'nav-Edit' || templateName === 'nav-Add') {
            return 'views/site/nav/' + templateName + '.html';
        } else if (templateName === 'content-list' || templateName === 'content-Edit' || templateName === 'content-Add') {
            return 'views/site/content/' + templateName + '.html';
        } else if (templateName === 'slider-list' || templateName === 'slider-Edit' || templateName === 'slider-Add') {
            return 'views/site/slider/' + templateName + '.html';
        } else if (templateName === 'uploader') {
            return 'views/util/' + templateName + '.html';
        } else if (templateName === 'blog-list' || templateName === 'blog-Edit' || templateName === 'blog-Add') {
            return 'views/site/blog/' + templateName + '.html';
        }else if (templateName === 'user-list' || templateName === 'user-Edit' || templateName === 'user-Add') {
            return 'views/people/user/' + templateName + '.html';
        }else if (templateName === 'category-list' || templateName === 'category-Edit' || templateName === 'category-Add') {
            return 'views/products/category/' + templateName + '.html';
        }else if (templateName === 'color-list' || templateName === 'color-Edit' || templateName === 'color-Add') {
            return 'views/products/color/' + templateName + '.html';
        }else if (templateName === 'size-list' || templateName === 'size-Edit' || templateName === 'size-Add') {
            return 'views/products/size/' + templateName + '.html';
        }else if (templateName === 'product-List' || templateName === 'product-Edit' || templateName === 'product-Add') {
            return 'views/products/product/' + templateName + '.html';
        }

    }
    $scope.getTemplateGroup = (templateName)=> {
        if (templateName==='dashboard') {
            return 'dashboard';
        }else if(templateName==='nav-list'||templateName==='nav-Edit'||templateName==='nav-Add'){
            return 'nav';
        }else if(templateName==='content-list'||templateName==='content-Edit'||templateName==='content-Add'){
            return 'content';
        }else if(templateName==='slider-list'||templateName==='slider-Edit'||templateName==='slider-Add'){
            return 'slider';
        }else if(templateName==='uploader'){
            return 'uploader';
        }else if(templateName==='blog-list'||templateName==='blog-Edit'||templateName==='blog-Add'){
            return 'blog';
        }else if(templateName==='user-list'||templateName==='user-Edit'||templateName==='user-Add'){
            return 'user';
        }else if(templateName==='category-list'||templateName==='category-Edit'||templateName==='category-Add'){
            return 'category';
        }else if(templateName==='color-list'||templateName==='color-Edit'||templateName==='color-Add'){
            return 'color';
        }else if(templateName==='size-List'||templateName==='size-Edit'||templateName==='size-Add'){
            return 'size';
        }else if(templateName==='product-List'||templateName==='product-Edit'||templateName==='product-Add'){
            return 'product';
        }
    }
    $scope.checkAccess();
});
