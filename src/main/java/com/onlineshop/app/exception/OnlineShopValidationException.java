package com.onlineshop.app.exception;

public class OnlineShopValidationException extends Exception{

    private Object[] params;
    public Object[] getParams() {
        return params;
    }

    public OnlineShopValidationException(String message) {
        super(message);
    }

    public OnlineShopValidationException(String message, Object[] params) {
        super(message);
        this.params = params;
    }
}
