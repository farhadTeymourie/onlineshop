package com.onlineshop.app.exception;

public class ServiceCallException extends Throwable{
    private Object[] params;

    public Object[] getParams() {
        return params;
    }

    public ServiceCallException(String message) {
        super(message);
    }

    public ServiceCallException(String message, Object... params) {
        this(message);
        this.params = params;
    }
}
