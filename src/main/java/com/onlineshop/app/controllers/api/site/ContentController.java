package com.onlineshop.app.controllers.api.site;

import com.onlineshop.app.entities.site.Content;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.services.site.ContentService;
import com.onlineshop.app.util.serviceresponse.ResponseStatus;
import com.onlineshop.app.util.serviceresponse.ServiceResponse;
import com.onlineshop.app.util.viewmodel.user.UserVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/content")
public class ContentController {

    @Autowired
    private ContentService contentService;

    @GetMapping("")
    public ServiceResponse<Content> find(@RequestParam String key){
        try{
            Content result = contentService.findByKey(key);
            return new ServiceResponse<>(ResponseStatus.SUCCESS,result);
        }catch (Exception e){
            return new ServiceResponse<>(e);
        }
    }

    @PostMapping("/")
    public ServiceResponse<Content> add(@RequestBody Content content) {
        try {
            Content contentData = contentService.add(content);
            return new ServiceResponse(ResponseStatus.SUCCESS, contentData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @PutMapping("/")
    public ServiceResponse<UserVM> update(@RequestBody Content content) {
        try {
            Content contentData = contentService.update(content);
            return new ServiceResponse(ResponseStatus.SUCCESS, contentData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @DeleteMapping("/{id}")
    public ServiceResponse<Boolean> delete(@PathVariable Long id) {
        try {
            Boolean result = contentService.deleteById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, result);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }
    @GetMapping("/{id}")
    public ServiceResponse<Content> search(@PathVariable Long id) {
        try {
            Content content = contentService.getContentById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, content);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }
    @GetMapping("/getAll")
    public ServiceResponse<Content> getAllData(
            @RequestParam Integer pageSize,
             @RequestParam Integer pageNumber){
        try{
            List<Content> result = contentService.getAllData(pageSize, pageNumber);
            long totalCount = contentService.getAllCount();
            return new ServiceResponse<>(ResponseStatus.SUCCESS,result, totalCount);
        }catch (Exception e){
            return new ServiceResponse<>(e);
        }
    }
}
