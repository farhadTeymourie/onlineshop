package com.onlineshop.app.controllers.api.pepole;

import com.onlineshop.app.entities.pepole.Customer;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.services.pepole.CustomerService;
import com.onlineshop.app.util.serviceresponse.ResponseStatus;
import com.onlineshop.app.util.serviceresponse.ServiceResponse;
import com.onlineshop.app.util.viewmodel.user.UserVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping("/")
    public ServiceResponse<Customer> add(@RequestBody Customer customer) {
        try {
            Customer customerData = customerService.add(customer);
            return new ServiceResponse(ResponseStatus.SUCCESS, customerData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);

        }
    }
    @PutMapping("/")
    public ServiceResponse<Customer> update(@RequestBody Customer customer) {
        try {
            Customer customerData = customerService.update(customer);
            return new ServiceResponse(ResponseStatus.SUCCESS, customerData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }
    @DeleteMapping("/{id}")
    public ServiceResponse<Boolean> delete(@PathVariable Long id) {
        try {
            Boolean result = customerService.deleteById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, result);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @GetMapping("/{id}")
    public ServiceResponse<UserVM> search(@PathVariable Long id) {
        try {
            Customer customer = customerService.getCustomerById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, customer);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }
}
