package com.onlineshop.app.controllers.api.site;

import com.onlineshop.app.entities.site.Nav;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.services.site.NavService;
import com.onlineshop.app.util.mapper.site.NavMapper;
import com.onlineshop.app.util.serviceresponse.ResponseStatus;
import com.onlineshop.app.util.serviceresponse.ServiceResponse;
import com.onlineshop.app.util.viewmodel.site.NavVm;
import com.onlineshop.app.util.viewmodel.user.UserVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/nav")
public class NavController {

    @Autowired
    private NavService navService;
    @Autowired
    private NavMapper navMapper;

    @PostMapping("/")
    public ServiceResponse<Nav> add(@RequestBody Nav nav) {
        try {
            Nav navData = navService.add(nav);
            return new ServiceResponse(ResponseStatus.SUCCESS, navData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @PutMapping("/")
    public ServiceResponse<UserVM> update(@RequestBody Nav nav) {
        try {
            Nav navData = navService.update(nav);
            return new ServiceResponse(ResponseStatus.SUCCESS, navData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @DeleteMapping("/{id}")
    public ServiceResponse<Boolean> delete(@PathVariable Long id) {
        try {
            Boolean result = navService.deleteById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, result);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @GetMapping("/getAll")
    public ServiceResponse<NavVm> getAll(
            @RequestParam Integer pageSize,
            @RequestParam Integer pageNumber) {
        try {
            List<Nav> result = navService.getAll(pageSize, pageNumber);
            List<NavVm> resultVM = new ArrayList<>();
            result.stream().forEach(x -> resultVM.add(navMapper.mapToVM(x)));
            long totalCount = navService.getAllCount();
            return new ServiceResponse<>(ResponseStatus.SUCCESS, resultVM, totalCount);
        } catch (Exception e) {
            return new ServiceResponse<>(e);
        }
    }

    @GetMapping("/{id}")
    public ServiceResponse<NavVm> getById(@PathVariable Long id) {
        Nav nav = null;
        try {
            nav = navService.getNavById(id);
            NavVm resultVm = navMapper.mapToVM(nav);
            return new ServiceResponse<>(ResponseStatus.SUCCESS, resultVm);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }
    @PostMapping("/changeOrder/{id}/{direction}")
    public ServiceResponse<Nav> changeOrder(@PathVariable long id, @PathVariable int direction) {
        try {
            Nav navData = navService.changeOrder(id, direction);
            return new ServiceResponse(ResponseStatus.SUCCESS, navData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }
}
