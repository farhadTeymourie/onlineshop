package com.onlineshop.app.controllers.api.products;

import com.onlineshop.app.entities.products.Feature;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.services.products.FeatureService;
import com.onlineshop.app.util.serviceresponse.ResponseStatus;
import com.onlineshop.app.util.serviceresponse.ServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/feature")
public class FeatureController {

    @Autowired
    private FeatureService featureService;

    @PostMapping("/")
    public ServiceResponse<Feature> add(@RequestBody Feature feature) {
        try {
            Feature featureData = featureService.add(feature);
            return new ServiceResponse(ResponseStatus.SUCCESS, featureData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @PutMapping("/")
    public ServiceResponse<Feature> update(@RequestBody Feature feature) {
        try {
            Feature featureData = featureService.update(feature);
            return new ServiceResponse(ResponseStatus.SUCCESS, featureData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @DeleteMapping("/{id}")
    public ServiceResponse<Boolean> delete(@PathVariable Long id) {
        try {
            Boolean result = featureService.deleteById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, result);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

//    @GetMapping("/{id}")
//    public ServiceResponse<Feature> search(@PathVariable Long id) {
//        try {
//            Feature feature = featureService.getFeatureById(id);
//            return new ServiceResponse(ResponseStatus.SUCCESS, feature);
//        } catch (OnlineShopValidationException e) {
//            return new ServiceResponse<>(e);
//        }
//    }
}
