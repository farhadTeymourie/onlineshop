package com.onlineshop.app.controllers.api.site;

import com.onlineshop.app.entities.site.Nav;
import com.onlineshop.app.entities.site.Slider;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.services.site.SliderService;
import com.onlineshop.app.util.mapper.site.SliderMapper;
import com.onlineshop.app.util.serviceresponse.ResponseStatus;
import com.onlineshop.app.util.serviceresponse.ServiceResponse;
import com.onlineshop.app.util.viewmodel.site.SliderVm;
import com.onlineshop.app.util.viewmodel.user.UserVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/slider")
public class SliderController {

    @Autowired
    private SliderService sliderService;
    @Autowired
    SliderMapper sliderMapper;

    @PostMapping("/")
    public ServiceResponse<Slider> add(@RequestBody Slider slider) {
        try {
            Slider sliderData = sliderService.add(slider);
            return new ServiceResponse(ResponseStatus.SUCCESS, sliderData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @PutMapping("/")
    public ServiceResponse<UserVM> update(@RequestBody Slider slider) {
        try {
            Slider sliderData = sliderService.update(slider);
            return new ServiceResponse(ResponseStatus.SUCCESS, sliderData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @DeleteMapping("/{id}")
    public ServiceResponse<Boolean> delete(@PathVariable Long id) {
        try {
            Boolean result = sliderService.deleteById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, result);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @GetMapping("/{id}")
    public ServiceResponse<Slider> search(@PathVariable Long id) {
        try {
            Slider slider = sliderService.getSliderById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, slider);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @GetMapping("")
    public ServiceResponse<Slider> get() {
        try {
            List<Slider> result = sliderService.findAllByOrderByItemOrder();
            return new ServiceResponse<>(ResponseStatus.SUCCESS, result);
        } catch (Exception e) {
            return new ServiceResponse<>(e);
        }
    }

    @GetMapping("/getAll")
    public ServiceResponse<SliderVm> getAll(
            @RequestParam Integer pageSize,
            @RequestParam Integer pageNumber) {
        try {
            List<Slider> result = sliderService.getAll(pageSize, pageNumber);
            List<SliderVm> resultVM = new ArrayList<>();
            result.stream().forEach(x -> resultVM.add(sliderMapper.mapToVM(x)));
            long totalCount = sliderService.getAllCount();
            return new ServiceResponse<>(ResponseStatus.SUCCESS, resultVM, totalCount);
        } catch (Exception e) {
            return new ServiceResponse<>(e);
        }
    }

    @PostMapping("/changeOrder/{id}/{direction}")
    public ServiceResponse<Nav> changeOrder(@PathVariable long id, @PathVariable int direction) {
        try {
            Slider sliderData = sliderService.changeOrder(id, direction);
            return new ServiceResponse(ResponseStatus.SUCCESS, sliderData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }
}
