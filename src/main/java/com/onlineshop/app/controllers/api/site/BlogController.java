package com.onlineshop.app.controllers.api.site;

import com.onlineshop.app.entities.site.Blog;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.services.site.BlogService;
import com.onlineshop.app.util.mapper.site.BlogMapper;
import com.onlineshop.app.util.serviceresponse.ResponseStatus;
import com.onlineshop.app.util.serviceresponse.ServiceResponse;
import com.onlineshop.app.util.viewmodel.site.BlogVm;
import com.onlineshop.app.util.viewmodel.user.UserVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/blog")
public class BlogController {

    @Autowired
    private BlogService blogService;
    @Autowired
    private BlogMapper blogMapper;

    @PostMapping("/")
    public ServiceResponse<Blog> add(@RequestBody Blog blog) {
        try {
            Blog blogData = blogService.add(blog);
            return new ServiceResponse(ResponseStatus.SUCCESS, blogData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @PutMapping("/")
    public ServiceResponse<UserVM> update(@RequestBody Blog blog) {
        try {
            Blog blogData = blogService.update(blog);
            return new ServiceResponse(ResponseStatus.SUCCESS, blogData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @DeleteMapping("/{id}")
    public ServiceResponse<Boolean> delete(@PathVariable Long id) {
        try {
            Boolean result = blogService.deleteById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, result);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }
    @PutMapping("/increaseVisit/{id}")
    public ServiceResponse<Blog> update(@PathVariable long id) {
        try {
            Blog result = blogService.increaseVisitCount(id);
            return new ServiceResponse<>(ResponseStatus.SUCCESS, result);
        } catch (Exception e) {
            return new ServiceResponse<>(e);
        }
    }

    @GetMapping("/{id}")
    public ServiceResponse<Blog> search(@PathVariable Long id) {
        try {
            Blog blog = blogService.getBlogById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, blog);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }
    @GetMapping("/info/{id}")
    public ServiceResponse<Blog> getInfo(@PathVariable long id) {
        try {
            Blog result = blogService.getBlogById(id);
            return new ServiceResponse<>(ResponseStatus.SUCCESS, result);
        } catch (Exception e) {
            return new ServiceResponse<>(e);
        }
    }
    @GetMapping("/getAll")
    public ServiceResponse<BlogVm> getAll(
            @RequestParam Integer pageSize,
            @RequestParam Integer pageNumber) {
        try {
            List<Blog> result = blogService.getAll(pageSize, pageNumber);
            List<BlogVm> resultVM = new ArrayList<>();
            result.stream().forEach(x -> resultVM.add(blogMapper.mapToVM(x)));
            long totalCount = blogService.getAllCount();
            return new ServiceResponse<>(ResponseStatus.SUCCESS, resultVM, totalCount);
        } catch (Exception e) {
            return new ServiceResponse<>(e);
        }
    }
}
