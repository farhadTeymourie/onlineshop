package com.onlineshop.app.controllers.api.products;

import com.onlineshop.app.entities.products.Product;
import com.onlineshop.app.entities.products.ProductCategory;
import com.onlineshop.app.entities.site.Slider;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.services.products.ProductCategoryService;
import com.onlineshop.app.services.products.ProductService;
import com.onlineshop.app.util.mapper.products.CategoryMapper;
import com.onlineshop.app.util.serviceresponse.ResponseStatus;
import com.onlineshop.app.util.serviceresponse.ServiceResponse;
import com.onlineshop.app.util.viewmodel.products.CategoryVm;
import com.onlineshop.app.util.viewmodel.site.SliderVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/category")
public class ProductCategoryController {

    @Autowired
    private ProductCategoryService service;
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private ProductService productService;

    @PostMapping("/")
    public ServiceResponse<ProductCategory> add(@RequestBody ProductCategory product) {
        try {
            ProductCategory productCategory = service.add(product);
            return new ServiceResponse(ResponseStatus.SUCCESS, productCategory);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @PutMapping("/")
    public ServiceResponse<ProductCategory> update(@RequestBody ProductCategory product) {
        try {
            ProductCategory productCategory = service.update(product);
            return new ServiceResponse(ResponseStatus.SUCCESS, productCategory);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

//    @DeleteMapping("/{id}")
//    public ServiceResponse<Boolean> delete(@PathVariable Long id) {
//        try {
//            List<Product> productList = productService.findByCategory(id);
//            if (productList.size() > 0) {
//                throw new OnlineShopValidationException("please remove product in this category first");
//            }
//            Boolean result = service.deleteById(id);
//            return new ServiceResponse(ResponseStatus.SUCCESS, result);
//        } catch (OnlineShopValidationException e) {
//            return new ServiceResponse<>(e);
//        }
//    }

    @GetMapping("/{id}")
    public ServiceResponse<ProductCategory> search(@PathVariable Long id) {
        try {
            ProductCategory productCategory = service.getProductCategoryById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, productCategory);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @GetMapping("/")
    public ServiceResponse<ProductCategory> get() {
        try {
            List<ProductCategory> productCategoryList = service.findAllByOrderByItemOrder();
            return new ServiceResponse(ResponseStatus.SUCCESS, productCategoryList);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @GetMapping("/getAll")
    public ServiceResponse<CategoryVm> getAll(
            @RequestParam Integer pageSize,
            @RequestParam Integer pageNumber) {
        try {
            List<ProductCategory> result = service.getAll(pageSize, pageNumber);
            List<CategoryVm> resultVM = new ArrayList<>();
            result.stream().forEach(x -> resultVM.add(categoryMapper.mapToVM(x)));
            long totalCount = service.getAllCount();
            return new ServiceResponse<>(ResponseStatus.SUCCESS, resultVM, totalCount);
        } catch (Exception e) {
            return new ServiceResponse<>(e);
        }
    }
}
