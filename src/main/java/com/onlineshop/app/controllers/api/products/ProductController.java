package com.onlineshop.app.controllers.api.products;

import com.onlineshop.app.entities.products.Product;
import com.onlineshop.app.entities.products.ProductCategory;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.services.products.ProductService;
import com.onlineshop.app.util.serviceresponse.ResponseStatus;
import com.onlineshop.app.util.serviceresponse.ServiceResponse;
import com.onlineshop.app.util.viewmodel.products.CategoryVm;
import com.onlineshop.app.util.viewmodel.products.ProductVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/getAll")
    public ServiceResponse<Product> getAll(
            @RequestParam Integer pageSize,
            @RequestParam Integer pageNumber) {
        try {
            List<Product> result = productService.getAll(pageSize, pageNumber);
            long totalCount = productService.getAllCount();
            return new ServiceResponse<>(ResponseStatus.SUCCESS, result, totalCount);
        } catch (Exception e) {
            return new ServiceResponse<>(e);
        }
    }

    @GetMapping("/getAll/{categoryId}")
    public ServiceResponse<Product> getAll(
            @RequestParam Integer pageSize,
            @RequestParam Integer pageNumber,
            @PathVariable long categoryId) {
        try {
            List<Product> result = productService.getAllByCategoryId(categoryId,pageSize, pageNumber);
            long totalCount = productService.getAllCountByCategoryId(categoryId);
            return new ServiceResponse<>(ResponseStatus.SUCCESS, result, totalCount);
        } catch (Exception e) {
            return new ServiceResponse<>(e);
        }
    }

    @PostMapping("/")
    public ServiceResponse<Product> add(@RequestBody ProductVM productVM) {
        try {
            Product productData = productService.add(productVM);
            return new ServiceResponse(ResponseStatus.SUCCESS, productData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @PutMapping("/")
    public ServiceResponse<Product> update(@RequestBody Product product) {
        try {
            Product productData = productService.update(product);
            return new ServiceResponse(ResponseStatus.SUCCESS, productData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @DeleteMapping("/{id}")
    public ServiceResponse<Boolean> delete(@PathVariable Long id) {
        try {
            Boolean result = productService.deleteById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, result);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @GetMapping("/{id}")
    public ServiceResponse<Product> getById(@PathVariable Long id) {
        try {
            Product product = productService.getProductById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, product);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }
//
//    @GetMapping("/find")
//    public ServiceResponse<Product> find(@RequestParam Long id) {
//        try {
//            List<Product> productList = productService.findByCategory(id);
//            return new ServiceResponse(ResponseStatus.SUCCESS, productList);
//        } catch (OnlineShopValidationException e) {
//            return new ServiceResponse<>(e);
//        }
//    }
}
