package com.onlineshop.app.controllers.api.orders;

import com.onlineshop.app.entities.orders.OrderItem;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.services.orders.OrderItemService;
import com.onlineshop.app.util.serviceresponse.ResponseStatus;
import com.onlineshop.app.util.serviceresponse.ServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/orderItem")
public class OrderItemController {

    @Autowired
    private OrderItemService service;

    @PostMapping("/")
    public ServiceResponse<OrderItem> add(@RequestBody OrderItem orderItem) {
        try {
            OrderItem orderItemData = service.add(orderItem);
            return new ServiceResponse(ResponseStatus.SUCCESS, orderItemData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @PutMapping("/")
    public ServiceResponse<OrderItem> update(@RequestBody OrderItem orderItem) {
        try {
            OrderItem orderItemData = service.update(orderItem);
            return new ServiceResponse(ResponseStatus.SUCCESS, orderItemData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @DeleteMapping("/{id}")
    public ServiceResponse<Boolean> delete(@PathVariable Long id) {
        try {
            Boolean result = service.deleteById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, result);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @GetMapping("/{id}")
    public ServiceResponse<OrderItem> search(@PathVariable Long id) {
        try {
            OrderItem orderItem = service.getOrderItemById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, orderItem);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }
}
