package com.onlineshop.app.controllers.api.products;

import com.onlineshop.app.entities.products.Colors;
import com.onlineshop.app.entities.site.Nav;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.services.products.ColorService;
import com.onlineshop.app.util.mapper.products.ColorMapper;
import com.onlineshop.app.util.serviceresponse.ResponseStatus;
import com.onlineshop.app.util.serviceresponse.ServiceResponse;
import com.onlineshop.app.util.viewmodel.products.ColorVm;
import com.onlineshop.app.util.viewmodel.site.NavVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/color")
public class ColorController {

    @Autowired
    private ColorService colorService;
    @Autowired
    private ColorMapper colorMapper;

    @GetMapping("/")
    public ServiceResponse<ColorVm> getAll() {
        try {
            List<Colors> result = colorService.getAll();
            List<ColorVm> resultVM = new ArrayList<>();
            result.stream().forEach(x -> resultVM.add(colorMapper.mapToVM(x)));
            long totalCount = colorService.getAllCount();
            return new ServiceResponse<>(ResponseStatus.SUCCESS, resultVM, totalCount);
        } catch (Exception e) {
            return new ServiceResponse<>(e);
        }
    }

    @GetMapping("/getAll")
    public ServiceResponse<ColorVm> getAll(
            @RequestParam Integer pageSize,
            @RequestParam Integer pageNumber) {
        try {
            List<Colors> result = colorService.getAll(pageSize, pageNumber);
            List<ColorVm> resultVM = new ArrayList<>();
            result.stream().forEach(x -> resultVM.add(colorMapper.mapToVM(x)));
            long totalCount = colorService.getAllCount();
            return new ServiceResponse<>(ResponseStatus.SUCCESS, resultVM, totalCount);
        } catch (Exception e) {
            return new ServiceResponse<>(e);
        }
    }

    @PostMapping("/")
    public ServiceResponse<Colors> add(@RequestBody Colors color) {
        try {
            Colors colorData = colorService.add(color);
            return new ServiceResponse(ResponseStatus.SUCCESS, colorData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @PutMapping("/")
    public ServiceResponse<Colors> update(@RequestBody Colors color) {
        try {
            Colors colorData = colorService.update(color);
            return new ServiceResponse(ResponseStatus.SUCCESS, colorData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @DeleteMapping("/{id}")
    public ServiceResponse<Boolean> delete(@PathVariable Long id) {
        try {
            Boolean result = colorService.deleteById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, result);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

//    @GetMapping("/{id}")
//    public ServiceResponse<Colors> search(@PathVariable Long id) {
//        try {
//            Colors color = colorService.getColorsById(id);
//            return new ServiceResponse(ResponseStatus.SUCCESS, color);
//        } catch (OnlineShopValidationException e) {
//            return new ServiceResponse<>(e);
//        }
//    }
}
