package com.onlineshop.app.controllers.api.orders;

import com.onlineshop.app.entities.orders.Invoice;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.services.orders.InvoiceService;
import com.onlineshop.app.util.serviceresponse.ResponseStatus;
import com.onlineshop.app.util.serviceresponse.ServiceResponse;
import com.onlineshop.app.util.viewmodel.user.UserVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/invoice")
public class InvoiceController {

    @Autowired
    private InvoiceService service;

    @PostMapping("/")
    public ServiceResponse<Invoice> add(@RequestBody Invoice invoice) {
        try {
            Invoice invoiceData = service.add(invoice);
            return new ServiceResponse(ResponseStatus.SUCCESS, invoiceData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @PutMapping("/")
    public ServiceResponse<UserVM> update(@RequestBody Invoice invoice) {
        try {
            Invoice invoiceData = service.update(invoice);
            return new ServiceResponse(ResponseStatus.SUCCESS, invoiceData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @DeleteMapping("/{id}")
    public ServiceResponse<Boolean> delete(@PathVariable Long id) {
        try {
            Boolean result = service.deleteById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, result);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @GetMapping("/{id}")
    public ServiceResponse<Invoice> search(@PathVariable Long id) {
        try {
            Invoice invoice = service.getInvoiceById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, invoice);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @GetMapping("/find")
    public ServiceResponse<Invoice> getByCustomerId(@PathVariable Long customerId) {
        try {
            List<Invoice> result = service.findByCustomer(customerId);
            return new ServiceResponse<>(ResponseStatus.SUCCESS, result);
        } catch (Exception e) {
            return new ServiceResponse<>(e);
        }
    }
}
