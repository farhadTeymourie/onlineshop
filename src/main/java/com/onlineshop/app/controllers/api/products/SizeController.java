package com.onlineshop.app.controllers.api.products;

import com.onlineshop.app.entities.products.Colors;
import com.onlineshop.app.entities.products.Size;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.services.products.SizeService;
import com.onlineshop.app.util.mapper.products.SizeMapper;
import com.onlineshop.app.util.serviceresponse.ResponseStatus;
import com.onlineshop.app.util.serviceresponse.ServiceResponse;
import com.onlineshop.app.util.viewmodel.products.ColorVm;
import com.onlineshop.app.util.viewmodel.products.SizeVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/size")
public class SizeController {

    @Autowired
    private SizeService sizeService;
    @Autowired
    private SizeMapper sizeMapper;

    @GetMapping("/")
    public ServiceResponse<SizeVm> getAll() {
        try {
            List<Size> result = sizeService.getAll();
            List<SizeVm> resultVM = new ArrayList<>();
            result.stream().forEach(x -> resultVM.add(sizeMapper.mapToVM(x)));
            long totalCount = sizeService.getAllCount();
            return new ServiceResponse<>(ResponseStatus.SUCCESS, resultVM, totalCount);
        } catch (Exception e) {
            return new ServiceResponse<>(e);
        }
    }

    @GetMapping("/getAll")
    public ServiceResponse<SizeVm> getAll(
            @RequestParam Integer pageSize,
            @RequestParam Integer pageNumber) {
        try {
            List<Size> result = sizeService.getAll(pageSize, pageNumber);
            List<SizeVm> resultVM = new ArrayList<>();
            result.stream().forEach(x -> resultVM.add(sizeMapper.mapToVM(x)));
            long totalCount = sizeService.getAllCount();
            return new ServiceResponse<>(ResponseStatus.SUCCESS, resultVM, totalCount);
        } catch (Exception e) {
            return new ServiceResponse<>(e);
        }
    }

    @PostMapping("/")
    public ServiceResponse<Size> add(@RequestBody Size size) {
        try {
            Size sizeData = sizeService.add(size);
            return new ServiceResponse(ResponseStatus.SUCCESS, sizeData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @PutMapping("/")
    public ServiceResponse<Size> update(@RequestBody Size size) {
        try {
            Size sizeData = sizeService.update(size);
            return new ServiceResponse(ResponseStatus.SUCCESS, sizeData);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @DeleteMapping("/{id}")
    public ServiceResponse<Boolean> delete(@PathVariable Long id) {
        try {
            Boolean result = sizeService.deleteById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, result);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

//    @GetMapping("/{id}")
//    public ServiceResponse<Size> search(@PathVariable Long id) {
//        try {
//            Size size = sizeService.getSizeById(id);
//            return new ServiceResponse(ResponseStatus.SUCCESS, size);
//        } catch (OnlineShopValidationException e) {
//            return new ServiceResponse<>(e);
//        }
//    }
}
