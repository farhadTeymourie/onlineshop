package com.onlineshop.app.controllers.api.pepole;

import com.onlineshop.app.config.JwtTokenUtil;
import com.onlineshop.app.entities.pepole.Users;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.services.pepole.UserService;
import com.onlineshop.app.util.mapper.pepole.UserMapper;
import com.onlineshop.app.util.securityUtils.UserInfo;
import com.onlineshop.app.util.serviceresponse.ResponseStatus;
import com.onlineshop.app.util.serviceresponse.ServiceResponse;
import com.onlineshop.app.util.viewmodel.user.UserVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserMapper userMapper;
    @Autowired
    private UserService userService;
    @Autowired
    JwtTokenUtil jwtTokenUtil;
    @Autowired
    UserInfo userInfo;

    @PostMapping("/login")
    public ServiceResponse<UserVM> login(@RequestBody Users user) {
        Users userData = userService.auth(user.getUserName(), user.getPassword());
        if (userData == null)
            return new ServiceResponse(ResponseStatus.FAILED, "invalid userName or password");
        UserVM userVM = userMapper.mapToVM(userData);
        String token = jwtTokenUtil.generateToken(userVM);
        userVM.setToken(token);
        return new ServiceResponse(ResponseStatus.SUCCESS, userVM);
    }

    @PostMapping("/")
    public ServiceResponse<UserVM> add(@RequestBody Users user) {
        try {
            Users userData = userService.add(user);
            UserVM userVM = userMapper.mapToVM(userData);
            return new ServiceResponse(ResponseStatus.SUCCESS, userVM);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }
    @PutMapping("/")
    public ServiceResponse<UserVM> update(@RequestBody Users user) {
        try {
            Users userData = userService.update(user);
            UserVM userVM = userMapper.mapToVM(userData);
            return new ServiceResponse(ResponseStatus.SUCCESS, userVM);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }
    @DeleteMapping("/{id}")
    public ServiceResponse<Boolean> delete(@PathVariable Long id) {
        try {
            Boolean result = userService.deleteById(id);
            return new ServiceResponse(ResponseStatus.SUCCESS, result);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }
    @PutMapping("/changePassword")
    public ServiceResponse<UserVM> changePassword(@RequestBody UserVM dataVm) {
        try {
            Users userData = userService.changePassword(dataVm.getId(),dataVm.getPassword(),dataVm.getNewPassword());
            UserVM userVM = userMapper.mapToVM(userData);
            return new ServiceResponse(ResponseStatus.SUCCESS, userVM);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }

    @GetMapping("/{id}")
    public ServiceResponse<UserVM> search(@PathVariable Long id) {
        try {
            Users userData = userService.getUsersById(id);
            UserVM userVM = userMapper.mapToVM(userData);
            return new ServiceResponse(ResponseStatus.SUCCESS, userVM);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }
    @GetMapping("/getUserInfo")
    public ServiceResponse<UserVM> getUserInfo(HttpServletRequest request) {
        try {
            String userName = userInfo.getUserInfoFromRequestTokenHeader(request);
            Users user = userService.getUserByUserName(userName);
            UserVM userVM = userMapper.mapToVM(user);
            return new ServiceResponse(ResponseStatus.SUCCESS, userVM);
        } catch (OnlineShopValidationException e) {
            return new ServiceResponse<>(e);
        }
    }
    @GetMapping("/getAll")
    public ServiceResponse<UserVM> getAll(
            @RequestParam Integer pageSize,
            @RequestParam Integer pageNumber) {
        try {
            List<Users> result = userService.getAll(pageSize, pageNumber);
            List<UserVM> resultVM = new ArrayList<>();
            result.stream().forEach(x -> resultVM.add(userMapper.mapToVM(x)));
            long totalCount = userService.getAllCount();
            return new ServiceResponse<>(ResponseStatus.SUCCESS, resultVM, totalCount);
        } catch (Exception e) {
            return new ServiceResponse<>(e);
        }
    }
}
