package com.onlineshop.app.enums;

public enum UserRole {
    ADMIN,
    USER
}
