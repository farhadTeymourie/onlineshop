package com.onlineshop.app.enums;

public enum BlogStatus {
    PUBLISHED,
    DRAFT,
}
