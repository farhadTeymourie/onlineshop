package com.onlineshop.app.config.filters;

import com.onlineshop.app.config.JwtTokenUtil;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.services.pepole.UserService;
import com.onlineshop.app.util.mapper.pepole.UserMapper;
import com.onlineshop.app.util.viewmodel.user.UserVM;
import io.jsonwebtoken.ExpiredJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Component
public class JwtRequestFilter implements Filter {

    @Autowired
    JwtTokenUtil jwtTokenUtil;
    @Autowired
    UserService userService;
    @Autowired
    UserMapper userMapper;
    private List<String> excludeUrls;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        excludeUrls = new ArrayList<>();
        excludeUrls.add("/api/user/login");
        excludeUrls.add("/api/color/");
        excludeUrls.add("/api/utils/upload/files/");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException ,ServletException  {
        try {
            String url = ((HttpServletRequest) request).getRequestURI().toLowerCase();
            if (excludeUrls.stream().anyMatch(x -> url.startsWith(x.toLowerCase()))) {
                chain.doFilter(request, response);
                return;
            }
            String requestTokenHeader = ((HttpServletRequest) request).getHeader("Authorization");
            if (requestTokenHeader == null || !requestTokenHeader.startsWith("Bearer ")) {
                log.info(ExceptionMessageCode.REQUEST_TOKEN_HEADER_DOES_NOT_SET);
                throw new OnlineShopValidationException("request token header dose not set");
            }
            String token = requestTokenHeader.substring(7);
            String username = jwtTokenUtil.getUsernameFromToken(token);
            if (username == null || username == "") {
                log.info(ExceptionMessageCode.USERNAME_CAN_NOT_RESOLVE);
                throw new OnlineShopValidationException("username can not resolve");
            }
            UserVM userVM = userMapper.mapToVM(userService.getUserByUserName(username));
            if (!jwtTokenUtil.validateToken(token, userVM)) {
                log.info(ExceptionMessageCode.INVALID_TOKEN);
                throw new OnlineShopValidationException("invakid token");
            }
            chain.doFilter(request, response);

        } catch (OnlineShopValidationException e) {
            ((HttpServletResponse) request).sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
        } catch (ExpiredJwtException e) {
            ((HttpServletResponse) request).sendError(HttpServletResponse.SC_EXPECTATION_FAILED, e.getMessage());
        } catch (Exception e) {
            ((HttpServletResponse) request).sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }
}
