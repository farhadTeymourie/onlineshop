package com.onlineshop.app.services.pepole;

import com.onlineshop.app.entities.pepole.Customer;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.repository.pepole.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public Customer getCustomerById(Long id) throws OnlineShopValidationException {
        if (id == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Customer);
        }
        return customerRepository.findById(id).orElse(new Customer());
    }

    public Customer add(Customer Customer) throws OnlineShopValidationException {
        if (Customer.getFirstName() == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Customer);
        }
        return customerRepository.save(Customer);
    }

    public Customer update(Customer Customer) throws OnlineShopValidationException {
        Customer oldCustomer = getCustomerById(Customer.getId());
        if (oldCustomer == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Customer + Customer.getId());
        }
        oldCustomer.setFirstName(Customer.getFirstName());
        oldCustomer.setLastName(Customer.getLastName());
        oldCustomer.setMobile(Customer.getMobile());
        oldCustomer.setEmail(Customer.getEmail());
        oldCustomer.setPostalCode(Customer.getPostalCode());
        oldCustomer.setAddress(Customer.getAddress());
        oldCustomer.setTel(Customer.getTel());
        return customerRepository.save(oldCustomer);
    }

    public Boolean deleteById(Long id) throws OnlineShopValidationException {
        Customer oldCustomer = getCustomerById(id);
        if (oldCustomer == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Customer + id);
        }
        customerRepository.deleteById(id);
        return true;
    }
}
