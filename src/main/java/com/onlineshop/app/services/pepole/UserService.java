package com.onlineshop.app.services.pepole;

import com.onlineshop.app.entities.pepole.Users;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.repository.pepole.UsersRepository;
import com.onlineshop.app.services.validation.UserValidation;
import com.onlineshop.app.util.securityUtils.EncryptPassword;
import com.onlineshop.app.util.viewmodel.user.UserVM;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private UserValidation validation;
    @Autowired
    private EncryptPassword encryptPassword;

    public Users getUserByUserName(String userName) throws OnlineShopValidationException {
        if (userName == null || userName == "") {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_firstName_is_null);
        }
        return usersRepository.findFirstByUserName(userName);
    }

    public Users auth(String userName, String password) {
        password = passwordEncrypted(password);
        return usersRepository.findFirstByUserNameAndPassword(userName, password);
    }

    public Users getUsersById(Long id) throws OnlineShopValidationException {
        Users user = usersRepository.findById(id).orElse(new Users());
        validation.getUsersByIdValidation(user);
        return user;
    }

    public Users add(Users user) throws OnlineShopValidationException {
        validation.userValidation(user);
        String password = passwordEncrypted(user.getPassword());
        user.setPassword(password);
        return usersRepository.save(user);
    }

    public Users update(Users users) throws OnlineShopValidationException {
        String passwordEncrypted = "";
        validation.getUsersByIdValidation(users);
        Users oldUsers = getUsersById(users.getId());
        if (users.getPassword() != null && users.getPassword() != "") {
            passwordEncrypted = passwordEncrypted(users.getPassword());
            oldUsers.setPassword(passwordEncrypted);
        }
        oldUsers.setFirstName(users.getFirstName());
        oldUsers.setLastName(users.getLastName());
        oldUsers.setEmail(users.getEmail());
        oldUsers.setEnable(users.isEnable());
        return usersRepository.save(oldUsers);
    }

    public Boolean deleteById(Long id) throws OnlineShopValidationException {
        Users oldUsers = getUsersById(id);
        if (oldUsers == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Users + id);
        }
        usersRepository.deleteById(id);
        return true;
    }

    public Users changePassword(Long id, String oldPassword, String newPassword) throws OnlineShopValidationException {
        Users user = getUsersById(id);
        oldPassword = passwordEncrypted(oldPassword);
        newPassword = passwordEncrypted(newPassword);
        if (user == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.USER_NOT_FOUND);
        }
        if (!(user.getPassword().equals(oldPassword))) {
            throw new OnlineShopValidationException(ExceptionMessageCode.INVALID_OLD_PASSWORD);
        }
        user.setPassword(newPassword);
        return usersRepository.save(user);
    }

    private String passwordEncrypted(String password) {
        try {
            return encryptPassword.encryptSHA1(password);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    public List<Users> getAll(Integer pageSize, Integer pageNumber) {
        Pageable page = PageRequest.of(pageNumber, pageSize, Sort.by("id"));
        Page<Users> all = usersRepository.findAll(page);
        return all.toList();
    }

    public long getAllCount() {
        return usersRepository.count();
    }

}
