package com.onlineshop.app.services.validation;

import com.onlineshop.app.entities.site.Nav;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import org.springframework.stereotype.Component;

@Component
public class NavValidation {
    public void navAddValidation(Nav nav) throws OnlineShopValidationException {
        if (nav == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Nav);
        }
        if (nav.getTitle() == null ||nav.getTitle().equals("")) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NAV_TITLE_CAN_NOT_BE_EMPTY);
        }
        if (nav.getLink() == null||nav.getLink().equals("")) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NAV_LINK_CAN_NOT_BE_EMPTY);
        }
    }

    public void navIdValidation(Long id) throws OnlineShopValidationException {
        if (id==null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NAV_ID_CAN_NOT_BE_EMPTY);
        }
    }
}
