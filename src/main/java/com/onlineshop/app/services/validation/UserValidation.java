package com.onlineshop.app.services.validation;

import com.onlineshop.app.entities.pepole.Users;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class UserValidation {
    public void getUsersByIdValidation( Users user) throws OnlineShopValidationException {
        if (user.getId()==null){
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Users );
        }
    }

    public void userValidation(Users user) throws OnlineShopValidationException {
        if (user.getFirstName() == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_firstName_is_null);
        }
        if (user.getUserName() == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_USERNAME_is_null);
        }
    }
}
