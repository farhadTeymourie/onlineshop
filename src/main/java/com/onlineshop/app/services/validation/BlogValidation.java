package com.onlineshop.app.services.validation;

import com.onlineshop.app.entities.site.Blog;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import org.springframework.stereotype.Component;

@Component
public class BlogValidation {
    public void validateBlog(Blog blog) throws OnlineShopValidationException {
        if (blog.getTitle() == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Blog);
        }
    }

    public void validateId(Blog blog) throws OnlineShopValidationException {
        if (blog.getId() == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Blog + blog.getId());
        }
    }
}
