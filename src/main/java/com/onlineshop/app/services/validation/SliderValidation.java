package com.onlineshop.app.services.validation;

import com.onlineshop.app.entities.site.Slider;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import org.springframework.stereotype.Component;

@Component
public class SliderValidation {
    public void sliderIdValidation(Long id) throws OnlineShopValidationException {
        if (id==null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NAV_ID_CAN_NOT_BE_EMPTY);
        }
    }

    public void sliderValidation(Slider slider) throws OnlineShopValidationException {
        if (slider == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_SLIDER );
        }
    }
}
