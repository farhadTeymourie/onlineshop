package com.onlineshop.app.services.products;

import com.onlineshop.app.entities.products.Colors;
import com.onlineshop.app.entities.site.Nav;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.repository.products.ColorsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ColorService {

    @Autowired
    private ColorsRepository colorsRepository;

    public Colors getColorsById(Long id)  {
        return colorsRepository.findById(id).orElse(new Colors());
    }

    public Colors add(Colors colors) throws OnlineShopValidationException {
        if (colors.getName() == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Colors);
        }
        return colorsRepository.save(colors);
    }

    public Colors update(Colors colors) throws OnlineShopValidationException {
        Colors oldColors = getColorsById(colors.getId());
        if (oldColors == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Colors + colors.getId());
        }
        oldColors.setName(colors.getName());
        oldColors.setValue(colors.getValue());
        return colorsRepository.save(oldColors);
    }

    public Boolean deleteById(Long id) throws OnlineShopValidationException {
        Colors oldColors = getColorsById(id);
        if (oldColors == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Colors + id);
        }
        colorsRepository.deleteById(id);
        return true;
    }


    public List<Colors> getAll(Integer pageSize, Integer pageNumber) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("id"));
        Page<Colors> all = colorsRepository.findAll(pageRequest);
        return all.toList();
    }

    public List<Colors> getAll() {
       return colorsRepository.findAll();
    }

    public long getAllCount() {
        return colorsRepository.count();
    }
}
