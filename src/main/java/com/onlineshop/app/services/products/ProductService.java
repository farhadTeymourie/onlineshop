package com.onlineshop.app.services.products;

import com.onlineshop.app.entities.products.Colors;
import com.onlineshop.app.entities.products.Product;
import com.onlineshop.app.entities.products.Size;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.repository.products.ProductRepository;
import com.onlineshop.app.util.mapper.products.ProductMapper;
import com.onlineshop.app.util.viewmodel.products.ProductVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ColorService colorService;
    @Autowired
    private SizeService sizeService;
    @Autowired
    private FeatureService featureService;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductCategoryService productCategoryService;

    public List<Product> getAll(Integer pageSize, Integer pageNumber) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("id"));
        Page<Product> all = productRepository.findAll(pageRequest);
        return all.toList();
    }

    public List<Product> search(String keyWord) {
        return productRepository.findAllByEnableIsTrueAndTitleContainsOrDescriptionContains(keyWord);
    }

    public Product getProductById(Long id) throws OnlineShopValidationException {
        if (id == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Product);
        }
        return productRepository.findById(id).orElse(new Product());
    }

    public Product add(ProductVM productVM) throws OnlineShopValidationException {
        if (productVM.getTitle() == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Product);
        }
        Product product = productMapper.mapToEntity(productVM);
        product = findColorForAddToProduct(product, productVM);
        product = findSizeForAddToProduct(product, productVM);
        product = addFeatureInToProduct(product, productVM);
        product.setAddDate(new Date());
        product.setProductCategory(productCategoryService.getProductCategoryById(productVM.getCategoryId()));
        return productRepository.save(product);
    }

    private Product addFeatureInToProduct(Product product, ProductVM productVM) {
        if (productVM != null) {
            if (productVM.getFeatures() != null) {
                List<Long> longList = new ArrayList<>();
                productVM.getFeatures().forEach(x->longList.add(x.longValue()));
                longList.forEach(x->{
                    featureService.getFeatureById(x);
                });
            }
        }
        return product;
    }

    private Product findColorForAddToProduct(Product product, ProductVM productVM) {
        if (productVM != null) {
            if (productVM.getColors() != null) {
                List<Long> longList = new ArrayList<>();
                productVM.getColors().forEach(x -> longList.add(x.longValue()));
                longList.forEach(x -> {
                    product.addColor(colorService.getColorsById(x));
                });
            }
        }
        return product;
    }

    private Product findSizeForAddToProduct(Product product, ProductVM productVM) {
        if (productVM != null) {
            if (productVM.getSizes() != null) {
                List<Long> longList = new ArrayList<>();
                productVM.getSizes().forEach(x -> longList.add(x.longValue()));
                longList.forEach(x -> {
                    product.addSize(sizeService.getSizeById(x));
                });
            }
        }
        return product;
    }

    public Product update(Product product) throws OnlineShopValidationException {
        Product oldProduct = getProductById(product.getId());
        if (oldProduct == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Product + product.getId());
        }
        oldProduct.setDescription(product.getDescription());
        oldProduct.setEnable(product.isEnable());
        oldProduct.setImage(product.getImage());
        oldProduct.setTitle(product.getTitle());
        oldProduct.setExists(product.isExists());
        oldProduct.setPrice(product.getPrice());
        oldProduct.setColorsList(product.getColorsList());
        oldProduct.setFeatureList(product.getFeatureList());
        oldProduct.setSizeList(product.getSizeList());
        return productRepository.save(oldProduct);
    }

    public Boolean deleteById(Long id) throws OnlineShopValidationException {
        Product oldProduct = getProductById(id);
        if (oldProduct == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Product + id);
        }
        productRepository.deleteById(id);
        return true;
    }

    public Product increaseVisitCount(Long id) throws OnlineShopValidationException {
        Product oldProduct = getProductById(id);
        if (oldProduct == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Blog + id);
        }
        oldProduct.setVisitCount(oldProduct.getVisitCount() + 1);
        return productRepository.save(oldProduct);
    }

    public long getAllCount() {
        return productRepository.count();
    }

    public long getAllCountByCategoryId(long categoryId) {
        return productRepository.countByProductCategoryId(categoryId);
    }

    public List<Product> getAllByCategoryId(Long categoryId, Integer pageSize, Integer pageNumber) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("id"));
        Page<Product> all = productRepository.findAllByProductCategory(categoryId, pageRequest);
//        List<ProductVM> vmList = new ArrayList<>();
//        all.toList().forEach(x -> vmList.add(new ProductVM(x)));
        return all.toList();
    }

//    public List<Product> findByCategory(Long id) {
//       return productRepository.findByProductCategoryId(id);
//    }
}
