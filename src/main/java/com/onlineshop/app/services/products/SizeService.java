package com.onlineshop.app.services.products;

import com.onlineshop.app.entities.products.Colors;
import com.onlineshop.app.entities.products.Size;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.repository.products.SizeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SizeService {

    @Autowired
    private SizeRepository sizeRepository;

    public Size getSizeById(Long id)  {
        return sizeRepository.findById(id).orElse(new Size());
    }

    public Size add(Size size) throws OnlineShopValidationException {
        if (size.getTitle() == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Size );
        }
        return sizeRepository.save(size);
    }

    public Size update(Size size) throws OnlineShopValidationException {
        Size oldSize = getSizeById(size.getId());
        if (oldSize == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Size + size.getId());
        }
        oldSize.setTitle(size.getTitle());
        oldSize.setDescription(size.getDescription());
        return sizeRepository.save(oldSize);
    }

    public Boolean deleteById(Long id) throws OnlineShopValidationException {
        Size oldSize = getSizeById(id);
        if (oldSize == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Size + id);
        }
        sizeRepository.deleteById(id);
        return true;
    }

    public List<Size> getAll(Integer pageSize, Integer pageNumber) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("id"));
        Page<Size> all = sizeRepository.findAll(pageRequest);
        return all.toList();
    }
    public List<Size> getAll() {
    return sizeRepository.findAll();
    }

    public long getAllCount() {
        return sizeRepository.count();
    }
}
