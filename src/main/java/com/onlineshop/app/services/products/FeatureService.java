package com.onlineshop.app.services.products;

import com.onlineshop.app.entities.products.Feature;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.repository.products.FeatureRepository;
import com.onlineshop.app.repository.products.FeatureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FeatureService {

    @Autowired
    private FeatureRepository featureRepository;

    public Feature getFeatureById(Long id)  {
          return featureRepository.findById(id).orElse(new Feature());
    }

    public Feature add(Feature feature) throws OnlineShopValidationException {
        if (feature.getKey() == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Feature );
        }
        return featureRepository.save(feature);
    }

    public Feature update(Feature feature) throws OnlineShopValidationException {
        Feature oldFeature = getFeatureById(feature.getId());
        if (oldFeature == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Feature + feature.getId());
        }
        oldFeature.setKey(feature.getKey());
        oldFeature.setValue(feature.getValue());
        return featureRepository.save(oldFeature);
    }

    public Boolean deleteById(Long id) throws OnlineShopValidationException {
        Feature oldFeature = getFeatureById(id);
        if (oldFeature == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Feature + id);
        }
        featureRepository.deleteById(id);
        return true;
    }
}
