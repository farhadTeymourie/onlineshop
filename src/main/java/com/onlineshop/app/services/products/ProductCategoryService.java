package com.onlineshop.app.services.products;

import com.onlineshop.app.entities.products.ProductCategory;
import com.onlineshop.app.entities.site.Slider;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.repository.products.ProductCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductCategoryService {

    @Autowired
    private ProductCategoryRepository repository;

    public List<ProductCategory> findAllByOrderByItemOrder() throws OnlineShopValidationException {
        Sort sort = Sort.by("id");
        if (sort== null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_ProductCategory );
        }
        return repository.findAllByEnableIsTrue(sort);
    }

    public ProductCategory getProductCategoryById(Long id) throws OnlineShopValidationException {
        if (id == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_ProductCategory );
        }
        return repository.findById(id).orElse(new ProductCategory());
    }

    public ProductCategory add(ProductCategory productCategory) throws OnlineShopValidationException {
        if (productCategory.getTitle() == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_ProductCategory + productCategory.getId());
        }
        return repository.save(productCategory);
    }

    public ProductCategory update(ProductCategory ProductCategory) throws OnlineShopValidationException {
        ProductCategory oldProductCategory = getProductCategoryById(ProductCategory.getId());
        if (oldProductCategory == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_ProductCategory + ProductCategory.getId());
        }
        oldProductCategory.setDescription(ProductCategory.getDescription());
        oldProductCategory.setEnable(ProductCategory.isEnable());
        oldProductCategory.setImage(ProductCategory.getImage());
        oldProductCategory.setTitle(ProductCategory.getTitle());
        return repository.save(oldProductCategory);
    }

    public Boolean deleteById(Long id) throws OnlineShopValidationException {
        ProductCategory oldProductCategory = getProductCategoryById(id);
        if (oldProductCategory == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_ProductCategory + id);
        }
        repository.deleteById(id);
        return true;
    }

    public List<ProductCategory> getAll(Integer pageSize, Integer pageNumber) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("id"));
        Page<ProductCategory> all = repository.findAll(pageRequest);
        return all.toList();
    }

    public long getAllCount() {
        return repository.count();
    }
}
