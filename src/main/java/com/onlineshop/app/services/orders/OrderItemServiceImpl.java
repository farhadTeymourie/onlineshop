package com.onlineshop.app.services.orders;

import com.onlineshop.app.entities.orders.OrderItem;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.repository.orders.OrderItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderItemServiceImpl implements OrderItemService {

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Override
    public OrderItem getOrderItemById(Long id) throws OnlineShopValidationException {
        if (id == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_OrderItem);
        }
        return orderItemRepository.findById(id).orElse(new OrderItem());
    }

    @Override
    public OrderItem add(OrderItem orderItem) throws OnlineShopValidationException {
        if (orderItem == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_OrderItem);
        }
        return orderItemRepository.save(orderItem);
    }

    @Override
    public OrderItem update(OrderItem OrderItem) throws OnlineShopValidationException {
        OrderItem oldOrderItem = getOrderItemById(OrderItem.getId());
        if (oldOrderItem == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_OrderItem + OrderItem.getId());
        }
        oldOrderItem.setCount(OrderItem.getCount());
        oldOrderItem.setPrice(OrderItem.getPrice());
        return orderItemRepository.save(oldOrderItem);
    }

    @Override
    public Boolean deleteById(Long id) throws OnlineShopValidationException {
        OrderItem oldOrderItem = getOrderItemById(id);
        if (oldOrderItem == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_OrderItem + id);
        }
        orderItemRepository.deleteById(id);
        return true;
    }
}
