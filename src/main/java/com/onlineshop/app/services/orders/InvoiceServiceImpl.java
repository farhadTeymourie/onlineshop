package com.onlineshop.app.services.orders;

import com.onlineshop.app.entities.orders.Invoice;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.repository.orders.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Override
    public List<Invoice> findByCustomer(Long id) throws OnlineShopValidationException {
        if (id == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Invoice);
        }
        return invoiceRepository.findByCustomer(id);
    }

    @Override
    public Invoice getInvoiceById(Long id) throws OnlineShopValidationException {
        if (id == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Invoice);
        }
        return invoiceRepository.findById(id).orElse(new Invoice());
    }

    @Override
    public Invoice add(Invoice invoice) throws OnlineShopValidationException {
        if (invoice.getInvoiceDate() == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Invoice);
        }
        return invoiceRepository.save(invoice);
    }

    @Override
    public Invoice update(Invoice invoice) throws OnlineShopValidationException {
        Invoice oldInvoice = getInvoiceById(invoice.getId());
        if (oldInvoice == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Invoice + invoice.getId());
        }
        oldInvoice.setInvoiceDate(invoice.getInvoiceDate());
        oldInvoice.setPayDate(invoice.getPayDate());
        return invoiceRepository.save(oldInvoice);
    }

    @Override
    public Boolean deleteById(Long id) throws OnlineShopValidationException {
        Invoice oldInvoice = getInvoiceById(id);
        if (oldInvoice == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Invoice + id);
        }
        invoiceRepository.deleteById(id);
        return true;
    }
}
