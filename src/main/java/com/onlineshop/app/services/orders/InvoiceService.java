package com.onlineshop.app.services.orders;

import com.onlineshop.app.entities.orders.Invoice;
import com.onlineshop.app.exception.OnlineShopValidationException;

import java.util.List;

public interface InvoiceService {

    List<Invoice> findByCustomer(Long id) throws OnlineShopValidationException;

    Invoice getInvoiceById(Long id) throws OnlineShopValidationException;

    Invoice add(Invoice invoice) throws OnlineShopValidationException;

    Invoice update(Invoice invoice) throws OnlineShopValidationException;

    Boolean deleteById(Long id) throws OnlineShopValidationException;
}
