package com.onlineshop.app.services.orders;

import com.onlineshop.app.entities.orders.OrderItem;
import com.onlineshop.app.exception.OnlineShopValidationException;

public interface OrderItemService {
    OrderItem getOrderItemById(Long id) throws OnlineShopValidationException;

    OrderItem add(OrderItem orderItem) throws OnlineShopValidationException;

    OrderItem update(OrderItem OrderItem) throws OnlineShopValidationException;

    Boolean deleteById(Long id) throws OnlineShopValidationException;
}
