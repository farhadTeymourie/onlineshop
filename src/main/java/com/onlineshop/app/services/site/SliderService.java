package com.onlineshop.app.services.site;

import com.onlineshop.app.entities.site.Nav;
import com.onlineshop.app.entities.site.Slider;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.repository.site.SliderRepository;
import com.onlineshop.app.services.validation.SliderValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SliderService {

    @Autowired
    private SliderRepository sliderRepository;
    @Autowired
    private SliderValidation validation;

    public List<Slider> findAllByOrderByItemOrder() {
        return sliderRepository.findAllByEnableIsTrue(Sort.by("itemOrder"));
    }

    public Slider getSliderById(Long id) throws OnlineShopValidationException {
        if (id == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_SLIDER );
        }
        return sliderRepository.findById(id).orElse(new Slider());
    }

    public Slider add(Slider slider) throws OnlineShopValidationException {
        validation.sliderValidation(slider);
        Slider lastSlider = sliderRepository.findTopByOrderByOrdersDesc();
        if (lastSlider==null) {
            slider.setOrders(1);
        }
        if (lastSlider!=null&&lastSlider.getOrders()>=1){
            slider.setOrders(lastSlider.getOrders()+1);
        }
        return sliderRepository.save(slider);
    }

    public Slider update(Slider slider) throws OnlineShopValidationException {
        Slider oldSlider = getSliderById(slider.getId());
        if (oldSlider == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_SLIDER + slider.getId());
        }
        oldSlider.setDescription(slider.getDescription());
        oldSlider.setEnable(slider.isEnable());
        oldSlider.setImageName(slider.getImageName());
        oldSlider.setLink(slider.getLink());
        oldSlider.setTitle(slider.getTitle());
        return sliderRepository.save(oldSlider);
    }

    public Boolean deleteById(Long id) throws OnlineShopValidationException {
        Slider oldSlider = getSliderById(id);
        if (oldSlider == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_SLIDER + id);
        }
        sliderRepository.deleteById(id);
        return true;
    }

    public List<Slider> getAll(Integer pageSize, Integer pageNumber) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("orders"));
        Page<Slider> all = sliderRepository.findAll(pageRequest);
        return all.toList();
    }

    public long getAllCount() {
        return sliderRepository.count();
    }
    public Slider getNavById(Long id) throws OnlineShopValidationException {
        validation.sliderIdValidation(id);
        return sliderRepository.findById(id).orElse(new Slider());
    }

    public Slider changeOrder(long id, int direction) throws OnlineShopValidationException {
        validation.sliderIdValidation(id);
        Slider item = getNavById(id);
        switch (direction) {
            case 1:
                //up
                if (item.getOrders() <= 1)
                    return item;
                Slider siblingItem = sliderRepository.findTopByOrders(item.getOrders() - 1);
                if (siblingItem == null)
                    item.setOrders(item.getOrders() - 1);
                else {
                    item.setOrders(siblingItem.getOrders());
                    siblingItem.setOrders(item.getOrders() + 1);
                    sliderRepository.save(siblingItem);
                }
                break;
            case 0:
                //down
                Slider siblingItem2 = sliderRepository.findTopByOrders(item.getOrders() + 1);
                if (siblingItem2 == null) {
                    Slider lastOrderedItem = sliderRepository.findTopByOrderByOrdersDesc();
                    if (item.getOrders() < lastOrderedItem.getOrders())
                        item.setOrders(item.getOrders() + 1);
                } else {
                    item.setOrders(siblingItem2.getOrders());
                    siblingItem2.setOrders(item.getOrders() - 1);
                    sliderRepository.save(siblingItem2);
                }
                break;
        }
        sliderRepository.save(item);
        return item;
    }
}
