package com.onlineshop.app.services.site;

import com.onlineshop.app.entities.pepole.Users;
import com.onlineshop.app.entities.site.Nav;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.repository.site.NavRepository;
import com.onlineshop.app.repository.site.NavRepository;
import com.onlineshop.app.services.validation.NavValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.awt.print.Pageable;
import java.util.List;

@Service
public class NavService {

    @Autowired
    private NavRepository navRepository;
    @Autowired
    NavValidation validation;

    public List<Nav> findAllByOrderByItemOrder() {
        return navRepository.findAllByEnableIsTrue(Sort.by("itemOrder"));
    }

    public Nav getNavById(Long id) throws OnlineShopValidationException {
        validation.navIdValidation(id);
        return navRepository.findById(id).orElse(new Nav());
    }

    public Nav add(Nav nav) throws OnlineShopValidationException {
        validation.navAddValidation(nav);
        Nav lastNav = navRepository.findTopByOrderByItemOrderDesc();
        if (lastNav == null) {
            nav.setItemOrder(1);
        }
        if (lastNav != null && lastNav.getItemOrder() >= 1)
            nav.setItemOrder(lastNav.getItemOrder() + 1);
        return navRepository.save(nav);
    }

    public Nav update(Nav Nav) throws OnlineShopValidationException {
        Nav oldNav = getNavById(Nav.getId());
        if (oldNav == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Nav + Nav.getId());
        }
        oldNav.setEnable(Nav.isEnable());
        oldNav.setLink(Nav.getLink());
        oldNav.setTitle(Nav.getTitle());
        return navRepository.save(oldNav);
    }

    public Boolean deleteById(Long id) throws OnlineShopValidationException {
        Nav oldNav = getNavById(id);
        if (oldNav == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Nav + id);
        }
        navRepository.deleteById(id);
        return true;
    }

    public long getAllCount() {
        return navRepository.count();
    }

    public List<Nav> getAll(Integer pageSize, Integer pageNumber) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("itemOrder"));
        Page<Nav> all = navRepository.findAll(pageRequest);
        return all.toList();
    }


    public Nav changeOrder(long id, int direction) throws OnlineShopValidationException {
        validation.navIdValidation(id);
        Nav item = getNavById(id);
        switch (direction) {
            case 1:
                //up
                if (item.getItemOrder() <= 1)
                    return item;
                Nav siblingItem = navRepository.findTopByItemOrder(item.getItemOrder() - 1);
                if (siblingItem == null)
                    item.setItemOrder(item.getItemOrder() - 1);
                else {
                    item.setItemOrder(siblingItem.getItemOrder());
                    siblingItem.setItemOrder(item.getItemOrder() + 1);
                    navRepository.save(siblingItem);
                }
                break;
            case 0:
                //down
                Nav siblingItem2 = navRepository.findTopByItemOrder(item.getItemOrder() + 1);
                if (siblingItem2 == null) {
                    Nav lastOrderedItem = navRepository.findTopByOrderByItemOrderDesc();
                    if (item.getItemOrder() < lastOrderedItem.getItemOrder())
                        item.setItemOrder(item.getItemOrder() + 1);
                } else {
                    item.setItemOrder(siblingItem2.getItemOrder());
                    siblingItem2.setItemOrder(item.getItemOrder() - 1);
                    navRepository.save(siblingItem2);
                }
                break;
        }
        navRepository.save(item);
        return item;
    }
}
