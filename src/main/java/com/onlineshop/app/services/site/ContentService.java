package com.onlineshop.app.services.site;

import com.onlineshop.app.entities.site.Content;
import com.onlineshop.app.entities.site.Nav;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.repository.site.ContentRepository;
import com.onlineshop.app.repository.site.ContentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContentService {

    @Autowired
    private ContentRepository contentRepository;

    public List<Content>getAllData(Integer pageSize, Integer pageNumber){
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("id"));
        Page<Content> all = contentRepository.findAll(pageRequest);
        return all.toList();
      }
    public long getAllCount() {
        return contentRepository.count();
    }
    public Content findByKey(String key) throws OnlineShopValidationException {
        if (key == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Content );
        }
        return contentRepository.findFirstByKey(key);
    }

    public Content getContentById(Long id) throws OnlineShopValidationException {
        if (id == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Content );
        }
        return contentRepository.findById(id).orElse(new Content());
    }

    public Content add(Content content) throws OnlineShopValidationException {
        if (content.getKey() == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Content );
        }
        return contentRepository.save(content);
    }

    public Content update(Content content) throws OnlineShopValidationException {
        Content oldContent = getContentById(content.getId());
        if (oldContent == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Content + content.getId());
        }
        oldContent.setKey(content.getKey());
        oldContent.setValue(content.getValue());
        return contentRepository.save(oldContent);
    }

    public Boolean deleteById(Long id) throws OnlineShopValidationException {
        Content oldContent = getContentById(id);
        if (oldContent == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Content + id);
        }
        contentRepository.deleteById(id);
        return true;
    }

}
