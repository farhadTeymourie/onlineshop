package com.onlineshop.app.services.site;

import com.onlineshop.app.entities.site.Blog;
import com.onlineshop.app.entities.site.Slider;
import com.onlineshop.app.exception.ExceptionMessageCode;
import com.onlineshop.app.exception.OnlineShopValidationException;
import com.onlineshop.app.repository.site.BlogRepository;
import com.onlineshop.app.repository.site.BlogRepository;
import com.onlineshop.app.services.validation.BlogValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlogService {

    @Autowired
    private BlogRepository blogRepository;

    @Autowired
    private BlogValidation validation;

    public List<Blog> search(String keyWord) {
        return blogRepository.findAllByTitleContainsOrDescriptionContains(keyWord);
    }

    public Blog getBlogById(Long id) throws OnlineShopValidationException {
        if (id == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Blog);
        }
        return blogRepository.findById(id).orElse(new Blog());
    }

    public Blog add(Blog blog) throws OnlineShopValidationException {
        validation.validateBlog(blog);
        blog.setVisitCount(0L);
        return blogRepository.save(blog);
    }

    public Blog update(Blog blog) throws OnlineShopValidationException {
        validation.validateId(blog);
        Blog oldBlog = getBlogById(blog.getId());
        oldBlog.setDescription(blog.getDescription());
        oldBlog.setImage(blog.getImage());
        oldBlog.setTitle(blog.getTitle());
        oldBlog.setSubTitle(blog.getSubTitle());
        oldBlog.setStatus(blog.getStatus());
        return blogRepository.save(oldBlog);
    }

    public Boolean deleteById(Long id) throws OnlineShopValidationException {
        Blog oldBlog = getBlogById(id);
        if (oldBlog == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Blog + id);
        }
        blogRepository.deleteById(id);
        return true;
    }

    public Blog increaseVisitCount(Long id) throws OnlineShopValidationException {
        Blog oldBlog = getBlogById(id);
        if (oldBlog == null) {
            throw new OnlineShopValidationException(ExceptionMessageCode.ONLINE_SHOP_NOT_FOUND_Blog + id);
        }
        oldBlog.setVisitCount(oldBlog.getVisitCount() + 1);
        return blogRepository.save(oldBlog);
    }

    public List<Blog> getAll(Integer pageSize, Integer pageNumber) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize, Sort.by("publishDate"));
        Page<Blog> all = blogRepository.findAll(pageRequest);
        return all.toList();
    }

    public long getAllCount() {
        return blogRepository.count();
    }
}
