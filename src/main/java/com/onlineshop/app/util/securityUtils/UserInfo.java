package com.onlineshop.app.util.securityUtils;

import com.onlineshop.app.config.JwtTokenUtil;
import com.onlineshop.app.exception.OnlineShopValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class UserInfo {

    @Autowired
    @Lazy
    JwtTokenUtil jwtTokenUtil;

    public String getUserInfoFromRequestTokenHeader(HttpServletRequest request) throws OnlineShopValidationException {
        String requestTokenHeader = request.getHeader("Authorization");
        if (requestTokenHeader == null || !requestTokenHeader.startsWith("Bearer ")) {
            throw new OnlineShopValidationException("request token header dose not set");
        }
        String token = requestTokenHeader.substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        if (username == null || username == "") {
            throw new OnlineShopValidationException("username can not resolve");
        }
        return username;
    }
}
