package com.onlineshop.app.util.mapper.products;

import com.onlineshop.app.entities.products.ProductCategory;
import com.onlineshop.app.entities.site.Slider;
import com.onlineshop.app.util.viewmodel.products.CategoryVm;
import com.onlineshop.app.util.viewmodel.site.SliderVm;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper {
    public CategoryVm mapToVM(ProductCategory category) {
        CategoryVm vm = new CategoryVm();
        vm.setId(category.getId());
        vm.setTitle(category.getTitle());
        vm.setEnable(category.isEnable());
        vm.setImage(category.getImage());
        vm.setDescription(category.getDescription());
        return vm;
    }
}
