package com.onlineshop.app.util.mapper.products;

import com.onlineshop.app.entities.products.Size;
import com.onlineshop.app.util.viewmodel.products.SizeVm;
import org.springframework.stereotype.Component;

@Component
public class SizeMapper {
    public SizeVm mapToVM(Size size) {
        SizeVm vm = new SizeVm();
        vm.setId(size.getId());
        vm.setTitle(size.getTitle());
        vm.setDescription(size.getDescription());
        return vm;
    }
}
