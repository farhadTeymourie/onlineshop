package com.onlineshop.app.util.mapper.products;

import com.onlineshop.app.entities.products.Product;
import com.onlineshop.app.util.viewmodel.products.ProductVM;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {
    public Product mapToEntity(ProductVM productVM) {
        Product product = new Product();
        if (productVM!=null) {
            product.setId(productVM.getId());
            product.setTitle(productVM.getTitle());
            product.setDescription(productVM.getDescription());
            product.setPrice(productVM.getPrice());
            product.setImage(productVM.getImage());
            product.setExists(productVM.isExists());
            product.setEnable(productVM.isEnable());
            product.setAddDate(productVM.getAddDate());
            product.setVisitCount(productVM.getVisitCount());
        }
            return product;
    }
}
