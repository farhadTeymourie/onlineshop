package com.onlineshop.app.util.mapper.products;

import com.onlineshop.app.entities.products.Colors;
import com.onlineshop.app.util.viewmodel.products.ColorVm;
import org.springframework.stereotype.Component;

@Component
public class ColorMapper {

    public ColorVm mapToVM(Colors color) {
        ColorVm vm = new ColorVm();
        vm.setId(color.getId());
        vm.setName(color.getName());
        vm.setValue(color.getValue());
        return vm;
    }
}
