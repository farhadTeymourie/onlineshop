package com.onlineshop.app.util.mapper.site;

import com.onlineshop.app.entities.site.Slider;
import com.onlineshop.app.util.viewmodel.site.SliderVm;
import lombok.Data;
import org.springframework.stereotype.Service;

@Data
@Service
public class SliderMapper {

    public SliderVm mapToVM(Slider slider) {
        SliderVm vm = new SliderVm();
        vm.setId(slider.getId());
        vm.setTitle(slider.getTitle());
        vm.setLink(slider.getLink());
        vm.setEnable(slider.isEnable());
        vm.setImageName(slider.getImageName());
        vm.setOrders(slider.getOrders());
        vm.setDescription(slider.getDescription());
        return vm;
    }
}
