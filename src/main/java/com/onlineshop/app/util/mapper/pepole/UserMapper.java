package com.onlineshop.app.util.mapper.pepole;

import com.onlineshop.app.entities.pepole.Users;
import com.onlineshop.app.util.viewmodel.user.UserVM;
import lombok.Data;
import org.springframework.stereotype.Service;

@Data
@Service
public class UserMapper {

    public  UserVM mapToVM(Users user) {
        UserVM vm = new UserVM();
        vm.setId(user.getId());
        vm.setFirstName(user.getFirstName());
        vm.setLastName(user.getLastName());
        vm.setUserName(user.getUserName());
        vm.setEmail(user.getEmail());
        vm.setEnable(user.isEnable());
        vm.setRole(user.getRole());
        vm.setFullName(user.getFirstName()+" "+user.getLastName());
        return vm;
    }
}
