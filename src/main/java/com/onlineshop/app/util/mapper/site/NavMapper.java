package com.onlineshop.app.util.mapper.site;

import com.onlineshop.app.entities.site.Nav;
import com.onlineshop.app.util.viewmodel.site.NavVm;
import lombok.Data;
import org.springframework.stereotype.Service;

@Data
@Service
public class NavMapper {

    public NavVm mapToVM(Nav nav) {
        NavVm vm = new NavVm();
        vm.setId(nav.getId());
        vm.setTitle(nav.getTitle());
        vm.setLink(nav.getLink());
        vm.setEnable(nav.isEnable());
        vm.setItemOrder(nav.getItemOrder());
        return vm;
    }
}
