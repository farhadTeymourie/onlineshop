package com.onlineshop.app.util.mapper.site;

import com.onlineshop.app.entities.site.Blog;
import com.onlineshop.app.util.viewmodel.site.BlogVm;
import org.springframework.stereotype.Component;

@Component
public class BlogMapper {
    public BlogVm mapToVM(Blog blog) {
        BlogVm vm = new BlogVm();
        vm.setId(blog.getId());
        vm.setTitle(blog.getTitle());
        vm.setStatus(blog.getStatus());
        vm.setPublishDate(blog.getPublishDate());
        vm.setVisitCount(blog.getVisitCount());
        vm.setDescription(blog.getDescription());
        vm.setSubTitle(blog.getSubTitle());
        vm.setImage(blog.getImage());
        return vm;
    }
}
