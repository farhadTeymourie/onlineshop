package com.onlineshop.app.util.serviceresponse;

public enum ResponseStatus {
    SUCCESS,
    FAILED,
    EXCEPTION
}
