package com.onlineshop.app.entities.site;

import javax.persistence.*;

@Entity
@Table(name = "Nav")
public class Nav {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @Column(name ="title")
    private String title;
    @Column(name = "link")
    private String link;
    @Column(name = "enable")
    private boolean enable;
    @Column(name = "itemOrder")
    private int itemOrder;

    public Nav() {
    }

    public Nav(Long id, String title, String link) {
        this.id = id;
        this.title = title;
        this.link = link;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public int getItemOrder() {
        return itemOrder;
    }

    public void setItemOrder(int itemOrder) {
        this.itemOrder = itemOrder;
    }
}
