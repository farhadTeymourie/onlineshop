package com.onlineshop.app.entities.site;

import com.onlineshop.app.enums.BlogStatus;

import javax.persistence.*;

import java.util.Date;

@Entity
@Table(name = "Blog")
public class Blog {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @Column(name ="title")
    private String title;
    @Column(name ="sub_Title")
    private String subTitle;
    @Column(name ="description",length = 4000)
    private String description;
    @Column(name ="image")
    private String image;
    @Column(name ="visitCount")
    private Long visitCount;
    @Column(name ="publishDate")
    private Date publishDate;
    private BlogStatus status;

    public Blog() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getVisitCount() {
        return visitCount;
    }

    public void setVisitCount(Long visitCount) {
        this.visitCount = visitCount;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public BlogStatus getStatus() {
        return status;
    }

    public void setStatus(BlogStatus status) {
        this.status = status;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }
}
