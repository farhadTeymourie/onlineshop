package com.onlineshop.app.entities.site;

import javax.persistence.*;

@Entity
@Table(name = "Slider")
public class Slider {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @Column(name = "title")
    private String title;
    @Column(name = "link")
    private String link;
    @Column(name = "imageName")
    private String imageName;
    @Column(name = "description")
    private String description;
    @Column(name = "orders" ,nullable = true)
    private int orders;
    @Column(name = "enable")
    private boolean enable;

    public Slider() {
    }

    public Slider(Long id, String title, String link, String imageName, String description, int orders) {
        this.id = id;
        this.title = title;
        this.link = link;
        this.imageName = imageName;
        this.description = description;
        this.orders = orders;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getOrders() {
        return orders;
    }

    public void setOrders(int orders) {
        this.orders = orders;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
