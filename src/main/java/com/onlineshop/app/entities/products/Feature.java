package com.onlineshop.app.entities.products;

import javax.persistence.*;

@Entity
@Table(name = "Feature")
public class Feature {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @Column(name = "key")
    private String key;
    @Column(name = "value")
    private String value;

    public Feature() {
    }

    public Feature(Long id, String key, String value) {
        this.id = id;
        this.key = key;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
