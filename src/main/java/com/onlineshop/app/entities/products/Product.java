package com.onlineshop.app.entities.products;


import javax.persistence.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Product")
public class Product {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @Column(name = "title")
    private String title;
    @Column(name = "price")
    private Long price;
    @Column(name = "image")
    private String image;
    @Column(name = "description")
    private String description;
    @Column(name = "visitCount")
    private Long visitCount;
    @ManyToOne
    @JoinColumn(name = "productCategory_id")
    private ProductCategory productCategory;
    @ManyToMany
    private List<Colors> colorsList;
    @ManyToMany
    private List<Feature> featureList;
    @ManyToMany
    private List<Size> sizeList;
    @Column(name = "enable")
    private boolean enable;
    @Column(name = "exists")
    private boolean exists;
    @Column(name = "addDate")
    private Date addDate;

    public Product() {
    }

//    public Product(Long id, String title, Long price, String imageName, String description, Long visitCount,
//                   ProductCategory productCategory, List<Colors> colorsList,
//                   List<Feature> featureList, List<Size> sizeList) {
//        this.id = id;
//        this.title = title;
//        this.price = price;
//        this.image = imageName;
//        this.description = description;
//        this.visitCount = visitCount;
//        this.productCategory = productCategory;
//        this.colorsList = colorsList;
//        this.featureList = featureList;
//        this.sizeList = sizeList;
//    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getVisitCount() {
        return visitCount;
    }

    public void setVisitCount(Long visitCount) {
        this.visitCount = visitCount;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public List<Colors> getColorsList() {
        if(colorsList == null)
            colorsList = new ArrayList<>();
        return colorsList;
    }

    public void setColorsList(List<Colors> colorsList) {
        this.colorsList = colorsList;
    }

    public List<Feature> getFeatureList() {
        if(featureList == null)
            featureList = new ArrayList<>();
        return featureList;
    }

    public void setFeatureList(List<Feature> featureList) {
        this.featureList = featureList;
    }

    public List<Size> getSizeList() {
        if(sizeList == null)
            sizeList = new ArrayList<>();
        return sizeList;
    }

    public void setSizeList(List<Size> sizeList) {
        this.sizeList = sizeList;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public boolean isExists() {
        return exists;
    }

    public void setExists(boolean exists) {
        this.exists = exists;
    }

    public Date getAddDate() {
        return addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }

    public void removeColor(Long id) {
        Colors color = getColorsList().stream().filter(c -> c.getId() == id).findFirst().get();
        getColorsList().remove(color);
    }

    public void addColor(Colors color) {
        if (color != null)
        getColorsList().add(color);
    }

    public void removeFeature(Long id) {
        Feature feature = getFeatureList().stream().filter(f -> f.getId() == id).findFirst().get();
        getFeatureList().remove(feature);
    }

    public void addFeature(Feature feature) {
        getFeatureList().add(feature);
    }

    public void removeSize(Long id) {
        Size size = getSizeList().stream().filter(s -> s.getId() == id).findFirst().get();
        getSizeList().remove(size);
    }

    public void addSize(Size size) {
        if (size != null)
        getSizeList().add(size);
    }
}

