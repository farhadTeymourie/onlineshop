package com.onlineshop.app.entities.orders;

import com.onlineshop.app.entities.pepole.Customer;
import com.onlineshop.app.entities.products.Product;
import javax.persistence.*;

@Entity
@Table(name = "OrderItem")
public class OrderItem {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @OneToOne
    @JoinColumn(name = "product_id")
    private Product product;
    @OneToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;
    @Column(name = "count")
    private Long count;
    @Column(name = "price")
    private Long price;

    public OrderItem() {
    }

    public OrderItem(Long id, Product product, Customer customer, Long count, Long price) {
        this.id = id;
        this.product = product;
        this.customer = customer;
        this.count = count;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }
}
