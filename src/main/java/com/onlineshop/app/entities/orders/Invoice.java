package com.onlineshop.app.entities.orders;

import com.onlineshop.app.entities.pepole.Customer;
import javax.persistence.*;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Invoice")
public class Invoice {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @Column(name = "invoiceDate")
    private Date invoiceDate;
    @Column(name = "payDate")
    private Date payDate;
    @OneToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;
    @OneToMany
    @JoinColumn(name = "orderItem_id")
    private List<OrderItem> orderItems;

    public Invoice() {
    }

    public Invoice(Long id, Date invoiceDate, Date payDate, Customer customer, List<OrderItem> orderItems) {
        this.id = id;
        this.invoiceDate = invoiceDate;
        this.payDate = payDate;
        this.customer = customer;
        this.orderItems = orderItems;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }
}
