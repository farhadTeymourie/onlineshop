package com.onlineshop.app.repository.products;

import com.onlineshop.app.entities.products.Colors;
import com.onlineshop.app.entities.products.Feature;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeatureRepository extends PagingAndSortingRepository<Feature,Long> {
}
