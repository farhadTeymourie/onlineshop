package com.onlineshop.app.repository.products;

import com.onlineshop.app.entities.products.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product,Long> {

    @Query(value = "from Product where productCategory.id=:productCategory_id",
            countQuery = "select count (id) from Product where productCategory.id=:productCategory_id")
    Page<Product> findAllByProductCategory(Long productCategory_id, Pageable pageable);

    @Query("from Product where enable=true and( title like concat ('%',:search,'%') or" +
            " description like concat('%',:search,'%'))")
    List<Product>findAllByEnableIsTrueAndTitleContainsOrDescriptionContains(String search);

    @Query("select count(id) from Product where productCategory.id= :productCategory_id")
    long countByProductCategoryId(long productCategory_id);

    @Query("from Product where productCategory.id=:productCategory_id")
    List<Product> findByProductCategoryId(Long productCategory_id);
}
