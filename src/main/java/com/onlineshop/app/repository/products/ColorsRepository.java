package com.onlineshop.app.repository.products;

import com.onlineshop.app.entities.products.Colors;
import com.onlineshop.app.entities.site.Blog;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ColorsRepository extends PagingAndSortingRepository<Colors,Long> {
    @Override
    List<Colors> findAll();
}
