package com.onlineshop.app.repository.pepole;

import com.onlineshop.app.entities.pepole.Customer;
import com.onlineshop.app.entities.products.Colors;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends PagingAndSortingRepository<Customer,Long> {
}
