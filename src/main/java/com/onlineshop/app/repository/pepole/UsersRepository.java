package com.onlineshop.app.repository.pepole;

import com.onlineshop.app.entities.pepole.Users;
import com.onlineshop.app.entities.products.Colors;
import org.apache.catalina.User;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public interface UsersRepository extends PagingAndSortingRepository<Users,Long> {
    Users findFirstByUserNameAndPassword(String userName,String Password);
    Users findFirstByUserName(String userName);

}
