package com.onlineshop.app.repository.site;

import com.onlineshop.app.entities.site.Content;
import com.onlineshop.app.entities.site.Nav;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContentRepository extends PagingAndSortingRepository<Content, Long> {
    Content findFirstByKey(String key);
}
