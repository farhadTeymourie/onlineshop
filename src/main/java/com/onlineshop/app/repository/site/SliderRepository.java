package com.onlineshop.app.repository.site;

import com.onlineshop.app.entities.site.Nav;
import com.onlineshop.app.entities.site.Slider;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SliderRepository extends PagingAndSortingRepository<Slider,Long> {

    List<Slider>findAllByEnableIsTrue(Sort sort);

    Slider findTopByOrders(int order);

    Slider findTopByOrderByOrdersDesc();
}
