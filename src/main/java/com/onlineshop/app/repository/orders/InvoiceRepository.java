package com.onlineshop.app.repository.orders;

import com.onlineshop.app.entities.orders.Invoice;
import com.onlineshop.app.entities.products.Colors;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvoiceRepository extends PagingAndSortingRepository<Invoice,Long> {

    @Query("from Invoice where customer.id=:customerId")
    List<Invoice>findByCustomer(Long customerId);
}
