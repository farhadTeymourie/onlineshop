package com.onlineshop.app.repository.orders;

import com.onlineshop.app.entities.orders.OrderItem;
import com.onlineshop.app.entities.products.Colors;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderItemRepository extends PagingAndSortingRepository<OrderItem,Long> {
}
